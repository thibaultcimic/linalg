# Lina, the Linear Package in C++

First and foremost, thank you for your interest in contributing to Lina! This document provides guidelines for contributions to this project. Please read it carefully before contributing.

## Project Philosophy

Lina started as a side project with a simple yet ambitious goal: to create a C++ linear algebra library reminiscent of Eigen but designed to mimic the intuitive and direct nature of pen-and-paper mathematics. This library has **no specific goal, aim, or pretension to utility**. It is a space for experimentation, learning, and creativity in the domain of mathematical computing.

Given that Lina is fundamentally a project of exploration and is essentially "useless" in a conventional sense, we wholeheartedly encourage you to interact with it in any way you see fit. Feel free to copy it, modify it, and use it as a playground for your ideas. This project is about the freedom to experiment and learn, without the constraints of utility dictating the direction of development.

We welcome contributions from anyone who shares this vision and is interested in exploring the boundaries of mathematical expressions in programming. Whether you are fixing a bug, adding a new feature, or improving documentation, your contributions are welcome.
## Installation

To use Lina in your project, you can either add it as a git submodule or clone it directly. Below are the instructions for both methods:

### Using as a Git Submodule

1. Navigate to your project's root directory.
2. Add Lina as a submodule:
   ```sh
   git submodule add https://gitlab.com/thibaultcimic/lina.git external/Lina
   ```
3. Initialize and update the submodule:
   ```sh
   git submodule update --init --recursive
   ```
4. Include Lina in your project's build system (e.g., CMake):
   ```cmake
   add_subdirectory(external/Lina)
   target_link_libraries(your_project_name PRIVATE Lina)
   ```

### Cloning Directly

1. Clone the Lina repository:
   ```sh
   git clone https://gitlab.com/thibaultcimic/lina.git
   ```
2. Navigate to the Lina directory:
   ```sh
   cd Lina
   ```
3. Build and install Lina:

   ```sh
   cd build
   cmake -S ../ -B ./
   cmake --build ./
   ```

By following these steps, you can integrate Lina into your project and start leveraging its capabilities for mathematical computing.

To ensure that Lina remains focused on its goals and maintains a high standard of quality, we ask that all contributors adhere to the following coding rules. These are essential for contributions to be considered for inclusion in Lina:

1. **C++, and Only C++**
   - **Focus on C++**: Lina is deeply rooted in C++ and focuses on algorithmic efficiency. To maintain coherence and avoid unnecessary complexity, dependencies, especially those on libraries in other languages, are strongly discouraged and should generally be avoided.

2. **Compiler and Language Standards**
   - **Compiler Compatibility**: Your contributions should compile without warnings with GCC compiler version 11.4, using the following options: `-Wall`, `-Wextra`, `-Werror`, `-Wmaybe-uninitialized`, `-fsanitize=undefined`. It's essential that the compiler returns no warnings to ensure code quality and compatibility.

3. **Memory Management**
   - **Valgrind Checks**: We take memory management seriously. Please ensure that your contributions are free of memory leaks and other common memory issues. Run Valgrind with the options `--leak-check=full`, `--track-origins=yes` to verify that your code does not introduce memory problems.

By adhering to these standards, you help ensure that Lina remains a robust, efficient, and high-quality C++ library.

## How to Contribute

Contributing to Lina can take various forms:

- **Bug Reports**: If you find a bug, please open an issue in the repository. Include a detailed description of the bug, steps to reproduce it, and, if possible, a code snippet or an error message.

- **Feature Suggestions**: Have ideas for new features or improvements? Open an issue to suggest them. Describe the feature as clearly as possible, including any technical details and potential use cases.

- **Code Contributions**: Ready to contribute code? Great! Please follow these steps:
  1. Fork the repository.
  2. Create a new branch for your contribution (`git checkout -b feature/your_feature_name`).
  3. Commit your changes. Write clear, concise commit messages that explain the "why" behind your changes.
  4. Push to your fork and submit a pull request to the main repository.

## Coding Standards and Requirements

Lina is more about learning and experimentation, so feel free to follow your own coding practices. Here are my coding practices, which you are welcome to follow:

1. **Code Formatting**
   - **Code Formatting**
      - I use a `clang-format` file to format the code. You can find the `clang-format` file in the root directory of the project. Feel free to use it to maintain consistency in code formatting.

   - **Pre-push Hook**
      - To ensure code quality before pushing to the remote repository, I use a pre-push hook. This pre-push hook script ensures that the code meets the project's standards before it is pushed to the repository. Here's a breakdown of its functionality:
         - **Branch Check**: If the current branch is the master branch, the script performs several checks:
            - **Build Check**: It builds the project using GCC to ensure there are no compilation errors.
            - **Test Execution**: It runs the project's tests to verify that there are no test failures.
            - **Memory and Undefined Behavior Checks**: It uses Valgrind and UBSan to check for memory leaks and undefined behavior, respectively.
         - **Code Formatting and Push**: Regardless of the branch, the script formats the code to adhere to the project's style guidelines and then pushes the changes to the repository.

      This process helps maintain code quality and consistency across the project.

      To set it up, copy the script located at `hooks/pre-push` to your local `.git/hooks` directory and keep it named `pre-push`.

By following these practices, you help maintain the project's quality and facilitate its growth and evolution. However, remember that Lina is a space for learning and creativity, so don't hesitate to experiment and develop your own coding practices.

We look forward to your contributions and thank you for your commitment to improving Lina.

2. **Testing**
   - Lina places a strong emphasis on testing. Ensure that your contributions are accompanied by comprehensive tests. This includes adding tests for new features and running existing tests to ensure your changes do not break the library. Your contributions should ideally enhance the overall test coverage of the project.

3. **Documentation**
   - **Source File Documentation**: For each source file you contribute, create a corresponding `.md` file that explains the implementation details, the algorithms used, and the rationale behind your choices.
   - **Contextual Documentation**: Additionally, create `.md` files that provide broader context or link various parts of the project together. This helps in understanding the project as a whole and elucidating the function and purpose of each component within it.

Contributions that align with these standards and guidelines are immensely valuable to Lina. They help maintain the project's quality and facilitate its growth and evolution. We look forward to your contributions and thank you for your commitment to improving Lina.

## Questions?

If you have any questions or need further guidance, feel free to open an issue with your question or reach out directly to the project maintainers.

Thank you for contributing to Lina! Your efforts help make this project a creative and exploratory space for mathematical programming.
