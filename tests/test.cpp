// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#include "Builder/Dense.h"
#include "Builder/Operators.h"
#include "Builder/Sparse.h"
#include "Builder/SymmetricPositiveDefinite.h"
#include "Builder/Vector.h"
#include "Solver/ConjugateGradient.h"
#include "Splitter/DefaultSplitter.h"

#include <gtest/gtest.h>
#include <limits>
#include <string>
#include <vector>

// Test fixture for the main function
class MainTest : public ::testing::Test {
  protected:
    void SetUp() override {
        // Set up any necessary resources before each test
    }

    void TearDown() override {
        // Clean up any resources after each test
    }
};

// Test matrix constructor
TEST(Constructor, DynoMatrixConstructor) {
    auto        generatedSPD = dyno::matrix::dense::alphaDPlusBetaRRT(10, 1.0, 2.0);
    std::string filePath = "/home/tcimic/Projects/Lina/MatrixCollection/alphaDPlusBetaRRT_10_1_2.txt";
    generatedSPD.writeToTxt(filePath);

    dyno::matrix::Dense<double> readSPD(filePath);
    EXPECT_TRUE(generatedSPD == readSPD);

    auto        generatedDiagDom = dyno::matrix::sparse::diagDom(10, 2);
    std::string filePath2 = "/home/tcimic/Projects/Lina/MatrixCollection/diagDom_10_3.mtx";
    generatedDiagDom.writeToCOO(filePath2);

    auto readDiagDom = dyno::matrix::sparse::COO<double>(filePath2);
    EXPECT_TRUE(generatedDiagDom == readDiagDom);

    auto diagDomCSR = dyno::matrix::sparse::CSR<double>(generatedDiagDom);
    EXPECT_TRUE(diagDomCSR == readDiagDom);

    auto fv1 = dyno::matrix::sparse::COO<double>("/home/tcimic/Projects/Lina/MatrixCollection/fv1.mtx");
    auto fv1Bis = dyno::matrix::sparse::COO<double>("/home/tcimic/Projects/Lina/MatrixCollection/fv1_bis.mtx");
    EXPECT_TRUE(fv1 == fv1Bis);
}

TEST(Constructor, StatoMatrixConstructor) {
    auto        generatedSPD = stato::matrix::dense::alphaDPlusBetaRRT<10>(1.0, 2.0);
    std::string filePath = "/home/tcimic/Projects/Lina/MatrixCollection/alphaDPlusBetaRRT_10_1_2.txt";
    generatedSPD.writeToTxt(filePath);

    stato::matrix::Dense<double, 10, 10> readSPD(filePath);
    EXPECT_TRUE(generatedSPD == readSPD);

    auto        generatedDiagDom = stato::matrix::sparse::diagDom<10, 2>();
    std::string filePath2 = "/home/tcimic/Projects/Lina/MatrixCollection/diagDom_10_3.mtx";
    generatedDiagDom.writeToCOO(filePath2);

    auto readDiagDom = stato::matrix::sparse::COO<double, 10, 10>(filePath2);
    EXPECT_TRUE(generatedDiagDom == readDiagDom);

    auto diagDomCSR = stato::matrix::sparse::CSR<double, 10, 10>(generatedDiagDom);
    EXPECT_TRUE(diagDomCSR == readDiagDom);

    auto fv1 = stato::matrix::sparse::COO<double, 9604, 9604>("/home/tcimic/Projects/Lina/MatrixCollection/fv1.mtx");
    auto fv1Bis = stato::matrix::sparse::COO<double, 9604, 9604>("/home/tcimic/Projects/Lina/MatrixCollection/fv1_bis.mtx");
    EXPECT_TRUE(fv1 == fv1Bis);
}

// Test the Vector class
TEST(BasicLinear, DynoVectorDotProduct) {
    dyno::Vector<double> vec1(10, 1.0);
    dyno::Vector<double> vec2(10, 2.0);

    auto dotProduct = vec1 * vec2;
    EXPECT_NEAR(dotProduct, 20.0, std::numeric_limits<double>::epsilon());

    auto mat = vec1 * vec2.transpose();

    dyno::matrix::Dense<double> matBis(10, 10);
    for (size_t i0 = 0; i0 < 10; ++i0) {
        for (size_t i1 = 0; i1 < 10; ++i1) {
            matBis(i0, i1) = vec1(i0) * vec2(i1);
            EXPECT_NEAR(mat(i0, i1), matBis(i0, i1), std::numeric_limits<double>::epsilon());
        }
    }
}

// Test the Vector class with stato::Vector
TEST(BasicLinear, StatoVectorDotProduct) {
    constexpr size_t            SIZE = 10;
    stato::Vector<double, SIZE> vec1(1.0);
    stato::Vector<double, SIZE> vec2(2.0);

    auto dotProduct = vec1 * vec2;
    EXPECT_NEAR(dotProduct, 20.0, std::numeric_limits<double>::epsilon());

    auto mat = vec1 * vec2.transpose();

    stato::matrix::Dense<double, SIZE, SIZE> matBis;
    for (size_t i0 = 0; i0 < SIZE; ++i0) {
        for (size_t i1 = 0; i1 < SIZE; ++i1) {
            matBis(i0, i1) = vec1(i0) * vec2(i1);
        }
    }

    EXPECT_TRUE(mat == matBis);
}

TEST(BasicLinear, DynoMatrixVectorMultiplication) {

    std::string filePath0 = "/home/tcimic/Projects/Lina/MatrixCollection/bcsstk01.mtx";

    dyno::matrix::sparse::COO<double> cooBCSSTK01(filePath0);
    // cooBCSSTK01.show();
    dyno::matrix::sparse::CSR<double> csrBCSSTK01(cooBCSSTK01);

    dyno::Vector<double> vecOnesBCSSTK01(cooBCSSTK01.getRows(), 1.0);
    auto                 vecCOOBCSSTK01 = cooBCSSTK01 * vecOnesBCSSTK01;
    auto                 vecCSRBCSSTK01 = csrBCSSTK01 * vecOnesBCSSTK01;

    EXPECT_NEAR((vecCOOBCSSTK01 - vecCSRBCSSTK01).norm(), 0.0, static_cast<double>(1e-6));
    EXPECT_TRUE(vecCOOBCSSTK01 == vecCSRBCSSTK01);

    std::string filePath1 = "/home/tcimic/Projects/Lina/MatrixCollection/alphaDPlusBetaRRT_10_1_2.txt";

    dyno::matrix::Dense<double> dense10(filePath1);

    dyno::Vector<double> onesDense10(dense10.getRows(), 1.0);

    auto vecDense10 = dense10 * onesDense10;

    std::vector<double> vecSumLinesDense10(10, 0.0);
    for (size_t i0 = 0; i0 < dense10.getRows(); ++i0) {
        for (size_t i1 = 0; i1 < dense10.getRows(); ++i1) {
            vecSumLinesDense10[i0] += dense10(i0, i1);
        }
    }

    auto diff1 = (vecDense10 - dyno::Vector<double>(vecSumLinesDense10)).norm();

    EXPECT_NEAR(diff1, 0.0, std::numeric_limits<double>::epsilon());

    std::string filePath2 = "/home/tcimic/Projects/Lina/MatrixCollection/gr_30_30.mtx";

    dyno::matrix::sparse::COO<double> cooGR3030(filePath2);

    dyno::Vector<double> vecOnesGR3030(cooGR3030.getRows(), 1.0);

    auto vecGR3030 = cooGR3030 * vecOnesGR3030;

    std::vector<double> vecSumLinesGR3030(cooGR3030.getRows(), 0.0);
    for (size_t i0 = 0; i0 < cooGR3030.getRows(); ++i0) {
        for (size_t i1 = 0; i1 < cooGR3030.getCols(); ++i1) {
            vecSumLinesGR3030[i0] += cooGR3030(i0, i1);
        }
    }

    auto vecSum = dyno::Vector<double>(vecSumLinesGR3030);
    auto diff2 = (vecGR3030 - vecSum).norm();

    EXPECT_NEAR(diff2, 0.0, std::numeric_limits<double>::epsilon());
}

TEST(BasicLinear, StatoMatrixVectorMultiplication) {

    std::string filePath0 = "/home/tcimic/Projects/Lina/MatrixCollection/bcsstk01.mtx";

    stato::matrix::sparse::COO<double, 48, 48> cooBCSSTK01(filePath0);
    // cooBCSSTK01.show();
    stato::matrix::sparse::CSR<double, 48, 48> csrBCSSTK01(cooBCSSTK01);

    for (size_t i0 = 0; i0 < cooBCSSTK01.getCols(); ++i0) {
        for (size_t i1 = 0; i1 < cooBCSSTK01.getRows(); ++i1) {
            EXPECT_EQ(cooBCSSTK01(i0, i1), csrBCSSTK01(i0, i1));
        }
    }

    stato::Vector<double, 48> vecOnesBCSSTK01(1.0);
    auto                      vecCOOBCSSTK01 = cooBCSSTK01 * vecOnesBCSSTK01;
    auto                      vecCSRBCSSTK01 = csrBCSSTK01 * vecOnesBCSSTK01;

    EXPECT_TRUE(vecCOOBCSSTK01 == vecCSRBCSSTK01);

    std::string filePath1 = "/home/tcimic/Projects/Lina/MatrixCollection/alphaDPlusBetaRRT_10_1_2.txt";

    stato::matrix::Dense<double, 10, 10> dense10(filePath1);

    stato::Vector<double, 10> onesDense10(1.0);

    auto vecDense10 = dense10 * onesDense10;

    std::array<double, 10> vecSumLinesDense10 = {};
    for (size_t i0 = 0; i0 < dense10.getRows(); ++i0) {
        for (size_t i1 = 0; i1 < dense10.getRows(); ++i1) {
            vecSumLinesDense10[i0] += dense10(i0, i1);
        }
    }

    auto diff1 = (vecDense10 - stato::Vector<double, 10>(vecSumLinesDense10)).norm();

    EXPECT_NEAR(diff1, 0.0, std::numeric_limits<double>::epsilon());

    std::string filePath2 = "/home/tcimic/Projects/Lina/MatrixCollection/gr_30_30.mtx";

    stato::matrix::sparse::COO<double, 900, 900> cooGR3030(filePath2);

    stato::Vector<double, 900> vecOnesGR3030(1.0);

    auto vecGR3030 = cooGR3030 * vecOnesGR3030;

    std::array<double, 900> vecSumLinesGR3030 = {};
    for (size_t i0 = 0; i0 < cooGR3030.getRows(); ++i0) {
        for (size_t i1 = 0; i1 < cooGR3030.getCols(); ++i1) {
            vecSumLinesGR3030[i0] += cooGR3030(i0, i1);
        }
    }

    auto vecSum = stato::Vector<double, 900>(vecSumLinesGR3030);
    auto diff2 = (vecGR3030 - vecSum).norm();

    EXPECT_NEAR(diff2, 0.0, std::numeric_limits<double>::epsilon());
}

TEST(BasicLinear, DynoMatrixSum) {
    std::string filePath1 = "/home/tcimic/Projects/Lina/MatrixCollection/alphaDPlusBetaRRT_10_1_2.txt";

    dyno::matrix::Dense<double> dense10(filePath1);

    auto twoDense10 = dense10 + dense10;

    dyno::matrix::Dense<double> nullDense10(twoDense10.getRows(), twoDense10.getCols());

    for (size_t i0 = 0; i0 < dense10.getRows(); ++i0) {
        for (size_t i1 = 0; i1 < dense10.getCols(); ++i1) {
            nullDense10(i0, i1) = twoDense10(i0, i1) - 2.0 * dense10(i0, i1);
            EXPECT_NEAR(nullDense10(i0, i1), 0.0, std::numeric_limits<double>::epsilon());
        }
    }
}

TEST(BasicLinear, StatoMatrixSum) {
    std::string filePath1 = "/home/tcimic/Projects/Lina/MatrixCollection/alphaDPlusBetaRRT_10_1_2.txt";

    stato::matrix::Dense<double, 10, 10> dense10(filePath1);

    auto twoDense10 = dense10 + dense10;

    stato::matrix::Dense<double, 10, 10> nullDense10;

    for (size_t i0 = 0; i0 < dense10.getRows(); ++i0) {
        for (size_t i1 = 0; i1 < dense10.getCols(); ++i1) {
            nullDense10(i0, i1) = twoDense10(i0, i1) - 2.0 * dense10(i0, i1);
            EXPECT_NEAR(nullDense10(i0, i1), 0.0, std::numeric_limits<double>::epsilon());
        }
    }
}

TEST(Solver, DynoConjugateGradient) {

    auto generatedSPD = dyno::matrix::dense::alphaDPlusBetaRRT(100, 57.0, 3.14);

    dyno::Vector<double> sol(generatedSPD.getRows(), 5.0);
    dyno::Vector<double> rhs = generatedSPD * sol;
    auto                 rhsNorm = rhs.norm();
    dyno::Vector<double> initialGuess(generatedSPD.getRows(), 0.0);
    size_t               maxIter = 1000;
    double               tol = 1e-8;

    dyno::solvers::ConjugateGradient<dyno::matrix::Dense<double>> cg(generatedSPD, rhs, initialGuess, maxIter, tol);
    auto                                                                  solution = cg.run();

    auto finalResidual = (rhs - generatedSPD * solution).norm();

    EXPECT_TRUE(finalResidual < tol * rhsNorm);

    auto generatedDiagDom = dyno::matrix::sparse::diagDom(100, 3);

    dyno::Vector<double> sol2(generatedDiagDom.getRows(), 5.0);
    dyno::Vector<double> rhs2 = generatedDiagDom * sol2;
    auto                 rhsNorm2 = rhs2.norm();
    dyno::Vector<double> initialGuess2(generatedDiagDom.getRows(), 0.0);

    dyno::solvers::ConjugateGradient<dyno::matrix::sparse::COO<double>> cg2(generatedDiagDom, rhs2, initialGuess2, maxIter, tol);
    auto                                                                        solution2 = cg2.run();

    auto finalResidual2 = (rhs2 - generatedDiagDom * solution2).norm();

    EXPECT_TRUE(finalResidual2 < tol * rhsNorm2);

    auto mesh3e1 = dyno::matrix::sparse::COO<double>("/home/tcimic/Projects/Lina/MatrixCollection/mesh3e1.mtx");

    dyno::Vector<double> sol3(mesh3e1.getRows(), 5.0);
    dyno::Vector<double> rhs3 = mesh3e1 * sol3;
    auto                 rhsNorm3 = rhs3.norm();
    dyno::Vector<double> initialGuess3(mesh3e1.getRows(), 0.0);

    dyno::solvers::ConjugateGradient<dyno::matrix::sparse::COO<double>> cg3(mesh3e1, rhs3, initialGuess3, maxIter, tol);
    auto                                                                        solution3 = cg3.run();

    auto finalResidual3 = (rhs3 - mesh3e1 * solution3).norm();

    EXPECT_TRUE(finalResidual3 < tol * rhsNorm3);

    auto fv1 = dyno::matrix::sparse::COO<double>("/home/tcimic/Projects/Lina/MatrixCollection/fv1.mtx");

    dyno::Vector<double> sol4(fv1.getRows(), 5.0);
    dyno::Vector<double> rhs4 = fv1 * sol4;
    auto                 rhsNorm4 = rhs4.norm();
    dyno::Vector<double> initialGuess4(fv1.getRows(), 0.0);

    dyno::solvers::ConjugateGradient<dyno::matrix::sparse::COO<double>> cg4(fv1, rhs4, initialGuess4, maxIter, tol);
    auto                                                                        solution4 = cg4.run();

    auto finalResidual4 = (rhs4 - fv1 * solution4).norm();

    EXPECT_TRUE(finalResidual4 < tol * rhsNorm4);

    auto gr_30_30 = dyno::matrix::sparse::COO<double>("/home/tcimic/Projects/Lina/MatrixCollection/gr_30_30.mtx");

    dyno::Vector<double> sol5(gr_30_30.getRows(), 5.0);
    dyno::Vector<double> rhs5 = gr_30_30 * sol5;
    auto                 rhsNorm5 = rhs5.norm();
    dyno::Vector<double> initialGuess5(gr_30_30.getRows(), 0.0);

    dyno::solvers::ConjugateGradient<dyno::matrix::sparse::COO<double>> cg5(gr_30_30, rhs5, initialGuess5, maxIter, tol);
    auto                                                                        solution5 = cg5.run();

    auto finalResidual5 = (rhs5 - gr_30_30 * solution5).norm();

    EXPECT_TRUE(finalResidual5 < tol * rhsNorm5);
}

TEST(Solver, StatoConjugateGradient) {
    constexpr size_t SIZE = 100;

    auto generatedSPD = stato::matrix::dense::alphaDPlusBetaRRT<SIZE>(57.0, 3.14);

    stato::Vector<double, SIZE> sol(5.0);
    stato::Vector<double, SIZE> rhs = generatedSPD * sol;
    auto                        rhsNorm = rhs.norm();
    stato::Vector<double, SIZE> initialGuess(0.0);
    size_t                      maxIter = 1000;
    double                      tol = 1e-8;

    stato::solvers::ConjugateGradient<stato::matrix::Dense<double, SIZE, SIZE>> cg(generatedSPD, rhs, initialGuess, maxIter, tol);
    auto                                                                                      solution = cg.run();

    auto finalResidual = (rhs - generatedSPD * solution).norm();

    EXPECT_TRUE(finalResidual < tol * rhsNorm);

    auto generatedDiagDom = stato::matrix::sparse::diagDom<SIZE, 3>();

    stato::Vector<double, SIZE> sol2(5.0);
    stato::Vector<double, SIZE> rhs2 = generatedDiagDom * sol2;
    auto                        rhsNorm2 = rhs2.norm();
    stato::Vector<double, SIZE> initialGuess2(0.0);

    stato::solvers::ConjugateGradient<stato::matrix::sparse::COO<double, SIZE, SIZE>> cg2(generatedDiagDom, rhs2, initialGuess2, maxIter, tol);
    auto                                                                                            solution2 = cg2.run();

    auto finalResidual2 = (rhs2 - generatedDiagDom * solution2).norm();

    EXPECT_TRUE(finalResidual2 < tol * rhsNorm2);

    auto mesh3e1 = stato::matrix::sparse::COO<double, 289, 289>("/home/tcimic/Projects/Lina/MatrixCollection/mesh3e1.mtx");

    stato::Vector<double, 289> sol3(5.0);
    stato::Vector<double, 289> rhs3 = mesh3e1 * sol3;
    auto                       rhsNorm3 = rhs3.norm();
    stato::Vector<double, 289> initialGuess3(0.0);

    stato::solvers::ConjugateGradient<stato::matrix::sparse::COO<double, 289, 289>> cg3(mesh3e1, rhs3, initialGuess3, maxIter, tol);
    auto                                                                                         solution3 = cg3.run();

    auto finalResidual3 = (rhs3 - mesh3e1 * solution3).norm();

    EXPECT_TRUE(finalResidual3 < tol * rhsNorm3);

    auto fv1 = stato::matrix::sparse::COO<double, 9604, 9604>("/home/tcimic/Projects/Lina/MatrixCollection/fv1.mtx");

    stato::Vector<double, 9604> sol4(5.0);
    stato::Vector<double, 9604> rhs4 = fv1 * sol4;
    auto                        rhsNorm4 = rhs4.norm();
    stato::Vector<double, 9604> initialGuess4(0.0);

    stato::solvers::ConjugateGradient<stato::matrix::sparse::COO<double, 9604, 9604>> cg4(fv1, rhs4, initialGuess4, maxIter, tol);
    auto                                                                                            solution4 = cg4.run();

    auto finalResidual4 = (rhs4 - fv1 * solution4).norm();

    EXPECT_TRUE(finalResidual4 < tol * rhsNorm4);

    auto gr_30_30 = stato::matrix::sparse::COO<double, 900, 900>("/home/tcimic/Projects/Lina/MatrixCollection/gr_30_30.mtx");

    stato::Vector<double, 900> sol5(5.0);
    stato::Vector<double, 900> rhs5 = gr_30_30 * sol5;
    auto                       rhsNorm5 = rhs5.norm();
    stato::Vector<double, 900> initialGuess5(0.0);

    stato::solvers::ConjugateGradient<stato::matrix::sparse::COO<double, 900, 900>> cg5(gr_30_30, rhs5, initialGuess5, maxIter, tol);
    auto                                                                                         solution5 = cg5.run();

    auto finalResidual5 = (rhs5 - gr_30_30 * solution5).norm();

    EXPECT_TRUE(finalResidual5 < tol * rhsNorm5);
}

TEST(DefaultSplitterTest, NonZeroPositions) {
    const size_t         N = 14;
    const size_t         splitFactor = 3;
    dyno::Vector<double> initialResidual(N, 1.0); // Fill the vector with ones for simplicity
    DefaultSplitter      splitter;

    // Perform the split operation
    auto splitMatrix = splitter.split(initialResidual, splitFactor);

    // Expected structure of the split matrix
    std::vector<std::vector<int>> expectedNonZeroIndices = {
        {0, 1, 2, 3, 4}, // Indices for the first column
        {5, 6, 7, 8, 9}, // Indices for the second column
        {10, 11, 12, 13} // Indices for the third column
    };

    // Check non-zero positions and zero positions
    for (size_t i = 0; i < N; ++i) {
        for (size_t j = 0; j < splitFactor; ++j) {
            if (std::find(expectedNonZeroIndices[j].begin(), expectedNonZeroIndices[j].end(), i) != expectedNonZeroIndices[j].end()) {
                EXPECT_NE(splitMatrix(i, j), 0.0) << "Expected non-zero at (" << i << ", " << j << ")";
            } else {
                EXPECT_EQ(splitMatrix(i, j), 0.0) << "Expected zero at (" << i << ", " << j << ")";
            }
        }
    }
}

// Run all the tests
int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
