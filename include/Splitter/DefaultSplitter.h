#pragma once

#include "Splitter/Splitter.h"

class DefaultSplitter : public Splitter {
  public:
    template <typename T> dyno::matrix::Dense<T> split(const dyno::Vector<T>& vector, size_t splitFactor) const {
        auto N = vector.size();
        auto q = N / splitFactor;
        auto r = N % splitFactor;

        dyno::matrix::Dense<T> splitMatrix(N, splitFactor);

        for (size_t i0 = 0; i0 < r; ++i0) {
            for (size_t i1 = 0; i1 < q + 1; ++i1) {
                splitMatrix(i1 + i0 * (q + 1), i0) = vector(i0 * (q + 1) + i1);
            }
        }

        for (size_t i0 = 0; i0 < splitFactor - r; ++i0) {
            for (size_t i1 = 0; i1 < q; ++i1) {
                splitMatrix(i1 + r * (q + 1) + i0 * q, i0 + r) = vector(r * (q + 1) + i1);
            }
        }

        return splitMatrix;
    };
};