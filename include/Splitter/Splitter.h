#pragma once

#include "Builder/Dense.h"
#include "Builder/Sparse.h"
#include "Builder/Vector.h"

class Splitter {
  public:
    virtual ~Splitter() = default;
};