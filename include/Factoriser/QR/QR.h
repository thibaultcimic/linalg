#pragma once

namespace lina {
    namespace factoriser {

    template <typename M, Q, R>
    class QR {
    public:
        virtual ~QR() = default;

        virtual std::pair<Q, R> factorize(const M& matrix) const = 0;

        // Optionally, a method to solve linear systems, assuming Q and R are known
        // virtual Vector<T, Cols> solve(const Vector<T, Rows>& b) const = 0;

        virtual const Matrix<T, Rows, Cols>& getQ() const = 0;
        virtual const Matrix<T, Cols, Cols>& getR() const = 0;
    };

    }  // namespace factoriser
}  // namespace lina