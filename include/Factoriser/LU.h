// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef LU_H
#define LU_H

#include "Builder/Dense.h"
#include "Builder/Sparse.h"

namespace factoriser {
    template <typename T> dyno::matrix::Dense<T> lu(const dyno::matrix::Dense<T>& A) {
        if (A.getRows() != A.getCols()) {
            throw std::invalid_argument("Matrix must be square.");
        }

        size_t                 n = A.getRows();
        dyno::matrix::Dense<T> L(n, n);
        dyno::matrix::Dense<T> U(n, n);

        for (size_t i = 0; i < n; ++i) {
            for (size_t j = 0; j < n; ++j) {
                if (i > j) {
                    L(i, j) = A(i, j);
                    U(i, j) = 0;
                } else if (i == j) {
                    L(i, j) = 1;
                    U(i, j) = A(i, j);
                } else {
                    L(i, j) = 0;
                    U(i, j) = A(i, j);
                }
            }
        }

        for (size_t k = 1; k < n; ++k) {
            for (size_t i = k; i < n; ++i) {
                L(i, k - 1) = U(i, k - 1) / U(k - 1, k - 1);
                for (size_t j = k - 1; j < n; ++j) {
                    U(i, j) -= L(i, k - 1) * U(k - 1, j);
                }
            }
        }

        return L;
    }

} // namespace factoriser

#endif // LU_H