#ifndef DENSE_H
#define DENSE_H

#include <cmath>
#include <cstdio>
#include <fstream>
#include <limits>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace dyno {

    template <typename T> class Vector;

    namespace matrix {

        template <typename T> class Dense {
          protected:
            size_t         rows;
            size_t         cols;
            std::vector<T> data;

          public:

            using scalarType = T;

            Dense(size_t rows, size_t cols);
            Dense(size_t rows, size_t cols, const std::vector<T>& data);
            Dense(T value, size_t rows, size_t cols);
            Dense(const std::string& filePath);
            ~Dense() = default;

            size_t                    getRows() const;
            size_t                    getCols() const;
            std::pair<size_t, size_t> size() const;

            T&                                           operator()(size_t i, size_t j);
            const T&                                     operator()(size_t i, size_t j) const;
            Dense<T>                                     operator+(const Dense& other) const;
            Dense<T>                                     operator-(const Dense& other) const;
            Dense<T>                                     operator*(const T& factor) const;
            template <typename U> friend dyno::Vector<U> operator*(const dyno::matrix::Dense<U>& matrix, const dyno::Vector<U>& vector);
            bool                                         operator==(const Dense& other) const;

            Dense<T> transpose() const;

            void writeToTxt(const std::string& filePath, const std::optional<int> precision = std::nullopt) const;
            void writeToCOO(const std::string& filePath, const std::optional<int> precision = std::nullopt) const;
            void show() const;
        };

        // Implementation of the dyno::Dense class

        template <typename T> Dense<T>::Dense(size_t rows, size_t cols) : rows(rows), cols(cols), data(std::vector<T>(rows * cols, T{})) {
        }

        template <typename T> Dense<T>::Dense(size_t rows, size_t cols, const std::vector<T>& data) : rows(rows), cols(cols), data(data) {
            if (data.size() != rows * cols) {
                throw std::invalid_argument("Data size does not match matrix dimensions");
            }
        }

        template <typename T> Dense<T>::Dense(T value, size_t rows, size_t cols) : rows(rows), cols(cols), data(rows * cols, value) {
        }

        template <typename T> Dense<T>::Dense(const std::string& filePath) {
            std::ifstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            std::vector<T> fileData;
            std::string    line;
            size_t         fileCols = 0;
            while (std::getline(file, line)) {
                std::istringstream iss(line);
                T                  value;
                size_t             lineCols = 0;
                while (iss >> value) {
                    fileData.push_back(value);
                    lineCols++;
                }
                if (fileCols == 0) {
                    fileCols = lineCols; // Set the number of columns based on the first line
                } else if (fileCols != lineCols) {
                    throw std::invalid_argument("Inconsistent number of columns in file");
                }
            }

            size_t fileRows = fileData.size() / fileCols;
            if (fileRows * fileCols != fileData.size()) {
                throw std::invalid_argument("Invalid matrix dimensions in file");
            }

            data = fileData;
            rows = fileRows;
            cols = fileCols; // Don't forget to set the number of columns
        }

        template <typename T> size_t Dense<T>::getRows() const {
            return rows;
        }

        template <typename T> size_t Dense<T>::getCols() const {
            return cols;
        }

        template <typename T> std::pair<size_t, size_t> Dense<T>::size() const {
            return std::make_pair(rows, cols);
        }

        template <typename T> T& Dense<T>::operator()(size_t i, size_t j) {
            if (i >= rows || j >= cols) {
                throw std::out_of_range("Matrix indices out of range");
            }
            return data[i * cols + j]; // row-major order
        }

        template <typename T> const T& Dense<T>::operator()(size_t i, size_t j) const {
            if (i >= rows || j >= cols) {
                throw std::out_of_range("Matrix indices out of range");
            }

            return data[i * cols + j]; // row-major order
        }

        template <typename T> Dense<T> Dense<T>::operator+(const Dense& other) const {
            if (rows != other.rows || cols != other.cols) {
                throw std::invalid_argument("Matrix dimensions do not match");
            }

            Dense result(rows, cols);

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    result.data[i * cols + j] = data[i * cols + j] + other.data[i * cols + j];
                }
            }

            return result;
        }

        template <typename T> Dense<T> Dense<T>::operator-(const Dense& other) const {
            if (rows != other.rows || cols != other.cols) {
                throw std::invalid_argument("Matrix dimensions do not match");
            }

            Dense result(rows, cols);

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    result.data[i * cols + j] = data[i * cols + j] - other.data[i * cols + j];
                }
            }

            return result;
        }

        template <typename T> Dense<T> Dense<T>::operator*(const T& factor) const {
            Dense result(rows, cols);

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    result.data[i * cols + j] = data[i * cols + j] * factor;
                }
            }

            return result;
        }

        template <typename T> bool Dense<T>::operator==(const Dense& other) const {
            if (rows != other.rows || cols != other.cols) {
                return false;
            }

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    if (std::abs(data[i * cols + j] - other.data[i * cols + j]) > std::numeric_limits<T>::epsilon() * 1000) {
                        // std::cerr << "Mismatch at (" << i << ", " << j << "): " << data[i * cols + j] << " != " << other.data[i * cols + j] << std::endl;
                        printf("Mismatch at (%lu, %lu): %10.20f != %10.20f\n", i, j, data[i * cols + j], other.data[i * cols + j]);
                        printf("Diff = %10.20f\n", std::abs(data[i * cols + j] - other.data[i * cols + j]));
                        return false;
                    }
                }
            }

            return true;
        }

        template <typename T> Dense<T> operator*(const T& factor, const Dense<T> matrix) {
            return matrix * factor;
        }

        template <typename T> Dense<T> Dense<T>::transpose() const {
            Dense result(cols, rows);

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    result.data[j * rows + i] = data[i * cols + j];
                }
            }

            return result;
        }

        template <typename T> void Dense<T>::writeToTxt(const std::string& filePath, const std::optional<int> precision) const {
            if (filePath.substr(filePath.size() - 4) != ".txt") {
                throw std::invalid_argument("Text file must have .txt extension");
            }

            std::ofstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            if (precision.has_value()) {
                file.precision(*precision);
            } else {
                int exp;
                std::frexp(std::numeric_limits<T>::epsilon(), &exp);
                file.precision(-exp);
            }

            file << std::fixed;

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    file << data[i * cols + j] << " ";
                }
                file << std::endl;
            }

            file.close();
        }

        template <typename T> void Dense<T>::writeToCOO(const std::string& filePath, const std::optional<int> precision) const {
            if (filePath.substr(filePath.size() - 4) != ".mtx") {
                throw std::invalid_argument("Matrix market file must have .mtx extension");
            }

            std::ofstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            if (precision.has_value()) {
                file.precision(*precision);
            } else {
                int exp;
                std::frexp(std::numeric_limits<T>::epsilon(), &exp);
                file.precision(-exp);
            }

            file << std::fixed;

            file << "%%MatrixMarket matrix coordinate real general" << std::endl;
            file << rows << " " << cols << " " << rows * cols << std::endl;

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    if (data[i * cols + j] != 0) {
                        file << i + 1 << " " << j + 1 << " " << data[i * cols + j] << std::endl;
                    }
                }
            }

            file.close();
        }

        template <typename T> void Dense<T>::show() const {
            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    printf("%10.2f ", static_cast<float>(data[i * cols + j]));
                }
                printf("\n");
            }
        }
    } // namespace matrix
} // namespace dyno

namespace stato {

    template <typename T, size_t N> class Vector;

    namespace matrix {

        template <typename T, size_t Rows, size_t Cols> class Dense {
          protected:
            std::array<T, Rows * Cols> data;

          public:

            using scalarType = T;
            static constexpr size_t rowsSize = Rows;
            static constexpr size_t colsSize = Cols;
          
            Dense();
            Dense(const std::array<T, Rows * Cols>& data);
            Dense(T value);
            Dense(const std::string& filePath);
            ~Dense() = default;

            static constexpr size_t getRows() {
                return Rows;
            }
            static constexpr size_t getCols() {
                return Cols;
            }
            static constexpr std::pair<size_t, size_t> size() {
                return std::make_pair(Rows, Cols);
            }

            T&                   operator()(size_t i, size_t j);
            const T&             operator()(size_t i, size_t j) const;
            Dense<T, Rows, Cols> operator+(const Dense& other) const;
            Dense<T, Rows, Cols> operator-(const Dense& other) const;
            Dense<T, Rows, Cols> operator*(const T& factor) const;
            template <typename U, size_t R, size_t C>
            friend stato::Vector<U, R> operator*(const stato::matrix::Dense<U, R, C>& matrix, const stato::Vector<U, C>& vector);
            bool                       operator==(const Dense& other) const;

            Dense<T, Cols, Rows> transpose() const;

            void writeToTxt(const std::string& filePath, const std::optional<int> precision = std::nullopt) const;
            void writeToCOO(const std::string& filePath, const std::optional<int> precision = std::nullopt) const;
            void show() const;
        };

        // Implementation of the stato::Dense class

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols>::Dense() {
            this->data.fill(T{});
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols>::Dense(const std::array<T, Rows * Cols>& data) : data(data) {
            if (data.size() != Rows * Cols) {
                throw std::invalid_argument("Data size does not match matrix dimensions");
            }
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols>::Dense(T value) {
            data.fill(value);
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols>::Dense(const std::string& filePath) {
            std::ifstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            std::array<T, Rows * Cols> fileData;
            std::string                line;
            size_t                     fileCols = 0;
            size_t                     index = 0;

            while (std::getline(file, line)) {
                std::istringstream iss(line);
                T                  value;
                size_t             lineCols = 0;

                while (iss >> value) {
                    if (index >= fileData.size()) {
                        throw std::invalid_argument("File contains more data than expected");
                    }
                    fileData[index++] = value;
                    lineCols++;
                }

                if (fileCols == 0) {
                    fileCols = lineCols; // Set the number of columns based on the first line
                } else if (fileCols != lineCols) {
                    throw std::invalid_argument("Inconsistent number of columns in file");
                }
            }

            size_t fileRows = index / fileCols;
            if (fileRows * fileCols != index) {
                throw std::invalid_argument("Invalid matrix dimensions in file");
            }

            if (fileRows != Rows || fileCols != Cols) {
                throw std::invalid_argument("File matrix dimensions do not match template parameters");
            }

            data = fileData;
        }

        template <typename T, size_t Rows, size_t Cols> T& Dense<T, Rows, Cols>::operator()(size_t i, size_t j) {
            if (i >= Rows || j >= Cols) {
                throw std::out_of_range("Matrix indices out of range");
            }
            return data[i * Cols + j]; // row-major order
        }

        template <typename T, size_t Rows, size_t Cols> const T& Dense<T, Rows, Cols>::operator()(size_t i, size_t j) const {
            if (i >= Rows || j >= Cols) {
                throw std::out_of_range("Matrix indices out of range");
            }

            return data[i * Cols + j]; // row-major order
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols> Dense<T, Rows, Cols>::operator+(const Dense& other) const {
            Dense<T, Rows, Cols> result{};

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    result.data[i * Cols + j] = data[i * Cols + j] + other.data[i * Cols + j];
                }
            }

            return result;
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols> Dense<T, Rows, Cols>::operator-(const Dense& other) const {
            Dense<T, Rows, Cols> result{};

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    result.data[i * Cols + j] = data[i * Cols + j] - other.data[i * Cols + j];
                }
            }

            return result;
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Rows, Cols> Dense<T, Rows, Cols>::operator*(const T& factor) const {
            Dense<T, Rows, Cols> result{};

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    result.data[i * Cols + j] = data[i * Cols + j] * factor;
                }
            }

            return result;
        }

        template <typename T, size_t Rows, size_t Cols> bool Dense<T, Rows, Cols>::operator==(const Dense& other) const {
            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    if (std::abs(data[i * Cols + j] - other.data[i * Cols + j]) > std::numeric_limits<T>::epsilon() * 1000) {
                        printf("Mismatch at (%lu, %lu): %10.20f != %10.20f\n", i, j, data[i * Cols + j], other.data[i * Cols + j]);
                        printf("Diff = %10.20f\n", std::abs(data[i * Cols + j] - other.data[i * Cols + j]));
                        return false;
                    }
                }
            }

            return true;
        }

        template <typename T, size_t Rows, size_t Cols> Dense<T, Cols, Rows> Dense<T, Rows, Cols>::transpose() const {
            Dense<T, Cols, Rows> result{};

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    result(j, i) = data[i * Cols + j];
                }
            }

            return result;
        }

        template <typename T, size_t Rows, size_t Cols>
        void Dense<T, Rows, Cols>::writeToTxt(const std::string& filePath, const std::optional<int> precision) const {
            if (filePath.substr(filePath.size() - 4) != ".txt") {
                throw std::invalid_argument("Text file must have .txt extension");
            }

            std::ofstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            if (precision.has_value()) {
                file.precision(*precision);
            } else {
                int exp;
                std::frexp(std::numeric_limits<T>::epsilon(), &exp);
                file.precision(-exp);
            }

            file << std::fixed;

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    file << data[i * Cols + j] << " ";
                }
                file << std::endl;
            }

            file.close();
        }

        template <typename T, size_t Rows, size_t Cols>
        void Dense<T, Rows, Cols>::writeToCOO(const std::string& filePath, const std::optional<int> precision) const {
            if (filePath.substr(filePath.size() - 4) != ".mtx") {
                throw std::invalid_argument("Matrix market file must have .mtx extension");
            }

            std::ofstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            if (precision.has_value()) {
                file.precision(*precision);
            } else {
                int exp;
                std::frexp(std::numeric_limits<T>::epsilon(), &exp);
                file.precision(-exp);
            }

            file << std::fixed;

            file << "%%MatrixMarket matrix coordinate real general" << std::endl;
            file << Rows << " " << Cols << " " << Rows * Cols << std::endl;

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    if (data[i * Cols + j] != 0) {
                        file << i + 1 << " " << j + 1 << " " << data[i * Cols + j] << std::endl;
                    }
                }
            }

            file.close();
        }

        template <typename T, size_t Rows, size_t Cols> void Dense<T, Rows, Cols>::show() const {
            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    printf("%10.2f ", static_cast<float>(data[i * Cols + j]));
                }
                printf("\n");
            }
        }

        // END Implementation of the stato::Dense class
    } // namespace matrix
} // namespace stato

#endif // DENSE_H