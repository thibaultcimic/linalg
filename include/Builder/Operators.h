// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef OPERATORS_H
#define OPERATORS_H

#include "Builder/Dense.h"
#include "Builder/Sparse.h"
#include "Builder/Vector.h"

namespace dyno::matrix {

    template <typename U> dyno::Vector<U> operator*(const dyno::matrix::Dense<U>& matrix, const dyno::Vector<U>& vector) {
        if (matrix.getCols() != vector.size()) {
            throw std::invalid_argument("Matrix and vector dimensions do not match");
        }

        dyno::Vector<U> result(matrix.getRows());

        for (size_t i = 0; i < matrix.getRows(); ++i) {
            for (size_t j = 0; j < matrix.getCols(); ++j) {
                result(i) += matrix(i, j) * vector(j);
            }
        }

        static_assert(std::is_same<decltype(result), dyno::Vector<U>>::value, "Result should be a Vector<U>");

        return result;
    }

    template <typename U> dyno::matrix::Dense<U> operator*(const dyno::Vector<U>& vector, const dyno::matrix::Dense<U>& matrix) {
        if (matrix.getRows() != 1 && vector.size() != matrix.getCols()) {
            throw std::invalid_argument("Vector and matrix dimensions do not match");
        }

        dyno::matrix::Dense<U> result(vector.size(), matrix.getCols());

        for (size_t i = 0; i < vector.size(); ++i) {
            for (size_t j = 0; j < matrix.getCols(); ++j) {
                result(j, i) += vector(j) * matrix(0, i);
            }
        }

        return result;
    }

    namespace sparse {

        template <typename U> dyno::Vector<U> operator*(const dyno::matrix::sparse::COO<U>& matrix, const dyno::Vector<U>& vector) {
            if (matrix.getCols() != vector.size()) {
                throw std::invalid_argument("Matrix and vector dimensions do not match");
            }

            std::cerr << "WARNING: You are doing matrix-vector multiplication with a COO matrix. Consider converting it to CSR for better performance." << std::endl;

            dyno::Vector<U> result(matrix.getRows(), U{});

            for (auto item : matrix.data) {
                auto row = item.first.first;
                auto col = item.first.second;
                auto value = item.second;
                result(row) += value * vector(col);
            }

            return result;
        }

        template <typename U> dyno::Vector<U> operator*(const dyno::matrix::sparse::CSR<U>& matrix, const dyno::Vector<U>& vector) {
            if (matrix.getCols() != vector.size()) {
                throw std::invalid_argument("Matrix and vector dimensions do not match");
            }

            dyno::Vector<U> result(matrix.getRows());

            for (size_t i = 0; i < matrix.getRows(); ++i) {
                for (size_t j = matrix.rowPtr[i]; j < matrix.rowPtr[i + 1]; ++j) {
                    result(i) += matrix.values[j] * vector(matrix.colInd[j]);
                }
            }

            return result;
        }
    } // namespace sparse
} // namespace dyno::matrix

namespace stato::matrix {

    template <typename U, size_t R, size_t C> stato::Vector<U, R> operator*(const stato::matrix::Dense<U, R, C>& matrix, const stato::Vector<U, C>& vector) {

        stato::Vector<U, R> result{};

        for (size_t i = 0; i < matrix.getRows(); ++i) {
            for (size_t j = 0; j < matrix.getCols(); ++j) {
                result(i) += matrix(i, j) * vector(j);
            }
        }

        static_assert(std::is_same<decltype(result), stato::Vector<U, R>>::value, "Result should be a Vector<U, R>");

        return result;
    }

    template <typename U, size_t C> stato::matrix::Dense<U, C, C> operator*(const stato::Vector<U, C>& vector, const stato::matrix::Dense<U, 1, C>& matrix) {

        stato::matrix::Dense<U, C, C> result;

        for (size_t i = 0; i < vector.size(); ++i) {
            for (size_t j = 0; j < matrix.getCols(); ++j) {
                result(i, j) += vector(i) * matrix(0, j);
            }
        }

        return result;
    }

    namespace sparse {

        template <typename U, size_t R, size_t C>
        stato::Vector<U, R> operator*(const stato::matrix::sparse::COO<U, R, C>& matrix, const stato::Vector<U, C>& vector) {

            std::cerr << "WARNING: You are doing matrix-vector multiplication with a COO matrix. Consider converting it to CSR for better performance." << std::endl;

            stato::Vector<U, R> result{};

            for (auto item : matrix.data) {
                auto row = item.first.first;
                auto col = item.first.second;
                auto value = item.second;
                result(row) += value * vector(col);
            }

            return result;
        }

        template <typename U, size_t R, size_t C>
        stato::Vector<U, R> operator*(const stato::matrix::sparse::CSR<U, R, C>& matrix, const stato::Vector<U, C>& vector) {

            stato::Vector<U, R> result{};

            for (size_t i = 0; i < matrix.getRows(); ++i) {
                for (size_t j = matrix.rowPtr[i]; j < matrix.rowPtr[i + 1]; ++j) {
                    result(i) += matrix.values[j] * vector(matrix.colInd[j]);
                }
            }

            return result;
        }

    } // namespace sparse
} // namespace stato::matrix

#endif // OPERATORS_H