// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef TRIANGULAR_H
#define TRIANGULAR_H

#include "Builder/Dense.h"
#include "Builder/Sparse.h"

namespace dyno::matrix {

    template <typename T> class Triangular : public Matrix<T> {
      private:
        bool   isLower;
        bool   isUnit;
        size_t n;

      public:
        Triangular(size_t n, bool isLower, bool isUnit);
        size_t getRows() const override;
        size_t getCols() const override;
        T      operator()(size_t i, size_t j) const override;
        T&     operator()(size_t i, size_t j) override;
        bool   operator==(const Matrix<T>& other) const override;
        bool   operator!=(const Matrix<T>& other) const override;
    };

    template <typename T> Triangular<T>::Triangular(size_t n, bool isLower, bool isUnit) : isLower(isLower), isUnit(isUnit), n(n) {
        if (n == 0) {
            throw std::invalid_argument("Matrix must have at least one row.");
        }
    }

    template <typename T> size_t Triangular<T>::getRows() const {
        return n;
    }

    template <typename T> size_t Triangular<T>::getCols() const {
        return n;
    }

    template <typename T> T Triangular<T>::operator()(size_t i, size_t j) const {
        if (i >= n || j >= n) {
            throw std::out_of_range("Index out of range.");
        }
        if (isLower && i < j) {
            return 0;
        }
        if (!isLower && i > j) {
            return 0;
        }
        if (isUnit && i == j) {
            return 1;
        }
        return 0;
    }

    template <typename T> T& Triangular<T>::operator()(size_t i, size_t j) {
        if (i >= n || j >= n) {
            throw std::out_of_range("Index out of range.");
        }
        if (isLower && i < j) {
            throw std::invalid_argument("Element is not in the matrix.");
        }
        if (!isLower && i > j) {
            throw std::invalid_argument("Element is not in the matrix.");
        }
        if (isUnit && i == j) {
            throw std::invalid_argument("Element is not in the matrix.");
        }
        return *new T(0);
    }

    template <typename T> bool Triangular<T>::operator==(const Matrix<T>& other) const {
        if (n != other.getRows() || n != other.getCols()) {
            return false;
        }
        for (size_t i = 0; i < n; ++i) {
            for (size_t j = 0; j < n; ++j) {
                if ((*this)(i, j) != other(i, j)) {
                    return false;
                }
            }
        }
        return true;
    }

    template <typename T> bool Triangular<T>::operator!=(const Matrix<T>& other) const {
        return !(*this == other);
    }
} // namespace dyno::matrix

#endif // TRIANGULAR_H