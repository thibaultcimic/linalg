#ifndef SPARSE_H
#define SPARSE_H

#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace dyno {

    template <typename T> class Vector;

    namespace matrix::sparse {

        template <typename T> class CSR;

        template <typename T> class COO {
          private:
            size_t                                 rows;
            size_t                                 cols;
            std::map<std::pair<size_t, size_t>, T> data;
            double                                 sparsity;

          public:

            using scalarType = T;

            COO(size_t rows, size_t cols);
            COO(size_t rows, size_t cols, const std::map<std::pair<size_t, size_t>, T>& data);
            COO(const std::string& filePath);
            COO(const dyno::matrix::Dense<T>& dense);
            ~COO() = default;
            friend CSR<T>::CSR(const COO<T>& coo);

            size_t getRows() const;
            size_t getCols() const;

            dyno::matrix::Dense<T> toDense() const;

            T                                            operator()(size_t i, size_t j) const;
            T&                                           operator()(size_t i, size_t j);
            COO<T>                                       operator+(const COO<T>& other) const;
            COO<T>                                       operator-(const COO<T>& other) const;
            template <typename U> friend dyno::Vector<U> operator*(const dyno::matrix::sparse::COO<U>& matrix, const dyno::Vector<U>& vector);
            bool                                         operator==(const COO<T>& other) const;

            void writeToCOO(const std::string& filePath, const std::optional<int> precision = std::nullopt) const;
            void show() const;
        };

        template <typename T> class CSR {
          private:
            std::vector<size_t> rowPtr;
            std::vector<size_t> colInd;
            std::vector<double> values;
            size_t              rows;
            size_t              cols;
            double              sparsity;

          public:

            using scalarType = T;

            CSR(size_t rows, size_t cols);
            CSR(std::vector<size_t> rowPtr, std::vector<size_t> colInd, std::vector<T> values, size_t rows, size_t cols);
            CSR(const std::string& filePath);
            CSR(const dyno::matrix::Dense<T>& dense);
            CSR(const COO<T>& coo);
            ~CSR() = default;

            size_t getRows() const;
            size_t getCols() const;
            size_t getSparsity() const;

            dyno::matrix::Dense<T> toDense() const;

            T                                            operator()(size_t i, size_t j) const;
            CSR<T>                                       operator+(const CSR<T>& other) const;
            CSR<T>                                       operator-(const CSR<T>& other) const;
            CSR<T>                                       operator*(const T& factor) const;
            CSR<T>                                       operator*(const CSR<T>& other) const;
            template <typename U> friend dyno::Vector<U> operator*(const CSR<U>& matrix, const dyno::Vector<U>& vector);
            bool                                         operator==(const CSR<T>& other) const;
        };

        // Implementation of the COO class

        template <typename T> COO<T>::COO(size_t rows, size_t cols) : rows(rows), cols(cols) {
        }

        template <typename T>
        COO<T>::COO(size_t rows, size_t cols, const std::map<std::pair<size_t, size_t>, T>& data)
            : rows(rows), cols(cols), data(data), sparsity(data.size() / (rows * cols)) {
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T> COO<T>::COO(const std::string& filePath) {
            std::ifstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            std::string line;
            size_t      numNonZero = 0;

            // Read the header line
            std::getline(file, line);
            bool symmetric = false;
            if (line.substr(0, 32) != "%%MatrixMarket matrix coordinate") {
                throw std::runtime_error("Invalid matrix market format");
            } else if (line.find(" symmetric") != std::string::npos) {
                symmetric = true;
            }

            // Skip comment lines
            while (std::getline(file, line) && line[0] == '%') {
            }

            // Read the matrix size and number of non-zero elements
            std::istringstream iss(line);
            iss >> rows >> cols >> numNonZero;

            // Resize the COO matrix
            data.clear();

            // Read the non-zero elements
            if (symmetric) {
                while (std::getline(file, line)) {
                    std::istringstream iss(line);
                    size_t             row, col;
                    T                  value;
                    iss >> row >> col >> value;
                    data[std::make_pair(row - 1, col - 1)] += value;
                    data[std::make_pair(col - 1, row - 1)] += value;
                }
            } else {
                while (std::getline(file, line)) {
                    std::istringstream iss(line);
                    size_t             row, col;
                    T                  value;
                    iss >> row >> col >> value;
                    data[std::make_pair(row - 1, col - 1)] += value;
                }
            }

            (*this).sparsity = static_cast<double>(numNonZero / (rows * cols));

            file.close();
        }

        template <typename T> COO<T>::COO(const dyno::matrix::Dense<T>& dense) : rows(dense.getRows()), cols(dense.getCols()) {
            for (size_t i = 0; i < dense.getRows(); ++i) {
                for (size_t j = 0; j < dense.getCols(); ++j) {
                    T value = dense(i, j);
                    if (value != 0) {
                        (*this)(i, j) = value;
                        ++sparsity;
                    }
                }
            }
            sparsity /= (rows * cols);
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T> size_t COO<T>::getRows() const {
            return rows;
        }

        template <typename T> size_t COO<T>::getCols() const {
            return cols;
        }

        template <typename T> dyno::matrix::Dense<T> COO<T>::toDense() const {
            Dense<T> dense(rows, cols);
            for (const auto& item : data) {
                dense(item.first.first, item.first.second) = item.second;
            }
            return dense;
        }

        template <typename T> T COO<T>::operator()(size_t i, size_t j) const {
            auto it = data.find({i, j});
            if (it != data.end()) {
                return it->second;
            }
            return T{}; // Return default value if element not found
        }

        template <typename T> T& COO<T>::operator()(size_t i, size_t j) {
            return data[{i, j}];
        }

        template <typename T> COO<T> COO<T>::operator+(const COO<T>& rhs) const {
            if (rows != rhs.getRows() || cols != rhs.getCols()) {
                throw std::invalid_argument("Matrix dimensions do not match");
            }

            COO<T> result(rows, cols);

            for (const auto& item : data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) = value;
            }

            for (const auto& item : rhs.data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) += value;
            }

            return result;
        }

        template <typename T> COO<T> COO<T>::operator-(const COO<T>& other) const {
            if (rows != other.getRows() || cols != other.getCols()) {
                throw std::invalid_argument("Matrix dimensions do not match");
            }

            COO<T> result(rows, cols);

            for (const auto& item : data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) = value;
            }

            for (const auto& item : other.data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) -= value;
            }

            return result;
        }

        template <typename T> bool COO<T>::operator==(const COO<T>& other) const {
            if (rows != other.rows || cols != other.cols) {
                return false;
            }

            for (const auto& item : data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                if (std::abs(value - other(row, col)) > std::numeric_limits<T>::epsilon() * 1000) {
                    std::cerr << "Mismatch at (" << row << ", " << col << "): " << value << " != " << other(row, col) << std::endl;
                    printf("Diff = %10.20f\n", std::abs(value - other(row, col)));
                    return false;
                }
            }

            return true;
        }

        template <typename T> COO<T> operator*(const T& factor, const COO<T> matrix) {
            return matrix * factor;
        }

        template <typename T> void COO<T>::writeToCOO(const std::string& filePath, const std::optional<int> precision) const {
            if (filePath.substr(filePath.size() - 4) != ".mtx") {
                throw std::invalid_argument("Matrix market file must have .mtx extension");
            }

            std::ofstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            if (precision.has_value()) {
                file.precision(*precision);
            } else {
                int exp;
                std::frexp(std::numeric_limits<T>::epsilon(), &exp);
                file.precision(-exp);
            }

            file << std::fixed;

            file << "%%MatrixMarket matrix coordinate real general" << std::endl;
            file << rows << " " << cols << " " << data.size() << std::endl;

            for (const auto& item : data) {
                file << item.first.first + 1 << " " << item.first.second + 1 << " " << item.second << std::endl;
            }

            file.close();
        }

        template <typename T> void COO<T>::show() const {
            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = 0; j < cols; ++j) {
                    printf("%10.2f ", static_cast<float>((*this)(i, j)));
                }
                printf("\n");
            }
        }

        // END Implementation of the COO class
        // Implementation of the CSR class

        template <typename T> CSR<T>::CSR(size_t rows, size_t cols) : rows(rows), cols(cols) {
        }

        template <typename T>
        CSR<T>::CSR(std::vector<size_t> rowPtr, std::vector<size_t> colInd, std::vector<T> values, size_t rows, size_t cols)
            : rowPtr(rowPtr), colInd(colInd), values(values), rows(rows), cols(cols), sparsity(values.size() / (rows * cols)) {
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T> CSR<T>::CSR(const std::string& filePath) {
            COO<T> coo(filePath);
            *this = CSR<T>::CSR(coo);
        }

        template <typename T> CSR<T>::CSR(const dyno::matrix::Dense<T>& dense) : rows(dense.getRows()), cols(dense.getCols()) {
            for (size_t i = 0; i < dense.getRows(); ++i) {
                for (size_t j = 0; j < dense.getCols(); ++j) {
                    T value = dense(i, j);
                    if (value != 0) {
                        (*this)(i, j) = value;
                        ++sparsity;
                    }
                }
            }
            sparsity /= (rows * cols);
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T> CSR<T>::CSR(const COO<T>& coo) : rows(coo.getRows()), cols(coo.getCols()) {
            std::map<size_t, std::map<size_t, T>> temp;
            for (const auto& item : coo.data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                temp[row][col] = value;
            }

            rowPtr.resize(rows + 1);
            colInd.clear();
            values.clear();

            size_t nnz = 0;
            for (size_t i = 0; i < rows; ++i) {
                rowPtr[i] = nnz;
                for (const auto& item : temp[i]) {
                    colInd.push_back(item.first);
                    values.push_back(item.second);
                    ++nnz;
                }
            }
            rowPtr[rows] = nnz;
            sparsity = coo.sparsity;
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T> size_t CSR<T>::getRows() const {
            return rows;
        }

        template <typename T> size_t CSR<T>::getCols() const {
            return cols;
        }

        template <typename T> size_t CSR<T>::getSparsity() const {
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << (*this).sparsity << std::endl;
            }
            return sparsity;
        }

        template <typename T> dyno::matrix::Dense<T> CSR<T>::toDense() const {
            Dense<T> dense(rows, cols);

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = rowPtr[i]; j < rowPtr[i + 1]; ++j) {
                    dense(i, colInd[j]) = values[j];
                }
            }
        }

        template <typename T> T CSR<T>::operator()(size_t i, size_t j) const {
            for (size_t k = rowPtr[i]; k < rowPtr[i + 1]; ++k) {
                if (colInd[k] == j) {
                    return values[k];
                }
            }

            return T{}; // Return default value if element not found
        }

        template <typename T> CSR<T> CSR<T>::operator+(const CSR<T>& rhs) const {
            throw std::runtime_error("CSR matrix shoud not be added directly, convert to COO or Dense matrix first");
        }

        template <typename T> CSR<T> CSR<T>::operator-(const CSR<T>& other) const {
            throw std::runtime_error("CSR matrix shoud not be subtracted directly, convert to COO or Dense matrix first");
        }

        template <typename T> CSR<T> CSR<T>::operator*(const T& factor) const {
            for (size_t i = 0; i < values.size(); ++i) {
                values[i] *= factor;
            }
        }

        template <typename T> CSR<T> CSR<T>::operator*(const CSR<T>& other) const {
            throw std::runtime_error("CSR matrix shoud not be multiplied directly, convert to COO or Dense matrix first");
        }

        template <typename T> CSR<T> operator*(const T& factor, const CSR<T> matrix) {
            return matrix * factor;
        }

        template <typename T> bool CSR<T>::operator==(const CSR<T>& other) const {
            if (rows != other.rows || cols != other.cols) {
                return false;
            }

            for (size_t i = 0; i < rows; ++i) {
                for (size_t j = rowPtr[i]; j < rowPtr[i + 1]; ++j) {
                    if (std::abs(values[j] - other(i, colInd[j])) > std::numeric_limits<T>::epsilon() * 1000) {
                        std::cerr << "Mismatch at (" << i << ", " << colInd[j] << "): " << values[j] << " != " << other(i, colInd[j]) << std::endl;
                        printf("Diff = %10.20f\n", std::abs(values[j] - other(i, colInd[j])));
                        return false;
                    }
                }
            }

            return true;
        }

        // END Implementation of the CSR class
    } // namespace matrix::sparse
} // namespace dyno

namespace stato {

    template <typename T, size_t N> class Vector;

    namespace matrix::sparse {

        template <typename T, size_t Rows, size_t Cols> class CSR;

        template <typename T, size_t Rows, size_t Cols> class COO {
          private:
            std::map<std::pair<size_t, size_t>, T> data;
            double                                 sparsity;

          public:

            using scalarType = T;
            static constexpr size_t rowsSize = Rows;
            static constexpr size_t colsSize = Cols;
            
            COO();
            COO(const std::map<std::pair<size_t, size_t>, T>& data);
            COO(const std::string& filePath);
            COO(const stato::matrix::Dense<T, Rows, Cols>& dense);
            ~COO() = default;
            friend CSR<T, Rows, Cols>::CSR(const COO<T, Rows, Cols>& coo);

            static constexpr size_t getRows() {
                return Rows;
            }
            static constexpr size_t getCols() {
                return Cols;
            }

            stato::matrix::Dense<T, Rows, Cols> toDense() const;

            T                  operator()(size_t i, size_t j) const;
            T&                 operator()(size_t i, size_t j);
            COO<T, Rows, Cols> operator+(const COO<T, Rows, Cols>& other) const;
            COO<T, Rows, Cols> operator-(const COO<T, Rows, Cols>& other) const;
            template <typename U, size_t R, size_t C>
            friend stato::Vector<U, R> operator*(const stato::matrix::sparse::COO<U, R, C>& matrix, const stato::Vector<U, C>& vector);
            bool                       operator==(const COO<T, Rows, Cols>& other) const;

            void writeToCOO(const std::string& filePath, const std::optional<int> precision = std::nullopt) const;
            void show() const;
        };

        template <typename T, size_t Rows, size_t Cols> class CSR {
          private:
            std::vector<size_t> rowPtr;
            std::vector<size_t> colInd;
            std::vector<double> values;
            double              sparsity;

          public:

            using scalarType = T;
            static constexpr size_t rowsSize = Rows;
            static constexpr size_t colsSize = Cols;

            CSR();
            CSR(std::vector<size_t> rowPtr, std::vector<size_t> colInd, std::vector<T> values);
            CSR(const std::string& filePath);
            CSR(const stato::matrix::Dense<T, Rows, Cols>& dense);
            CSR(const COO<T, Rows, Cols>& coo);
            ~CSR() = default;

            static constexpr size_t getRows() {
                return Rows;
            }
            static constexpr size_t getCols() {
                return Cols;
            }
            size_t getSparsity() const;

            stato::matrix::Dense<T, Rows, Cols> toDense() const;

            T                                                                    operator()(size_t i, size_t j) const;
            CSR<T, Rows, Cols>                                                   operator+(const CSR<T, Rows, Cols>& other) const;
            CSR<T, Rows, Cols>                                                   operator-(const CSR<T, Rows, Cols>& other) const;
            CSR<T, Rows, Cols>                                                   operator*(const T& factor) const;
            CSR<T, Rows, Cols>                                                   operator*(const CSR<T, Rows, Cols>& other) const;
            template <typename U, size_t R, size_t C> friend stato::Vector<U, R> operator*(const CSR<U, R, C>& matrix, const stato::Vector<U, C>& vector);
            bool                                                                 operator==(const CSR<T, Rows, Cols>& other) const;
        };

        // Implementation of the CSR class

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols>::CSR() {
        }

        template <typename T, size_t Rows, size_t Cols>
        CSR<T, Rows, Cols>::CSR(std::vector<size_t> rowPtr, std::vector<size_t> colInd, std::vector<T> values)
            : rowPtr(rowPtr), colInd(colInd), values(values), sparsity(values.size() / (Rows * Cols)) {
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols>::CSR(const std::string& filePath) {
            COO<T, Rows, Cols> coo(filePath);
            *this = CSR<T, Rows, Cols>::CSR(coo);
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols>::CSR(const stato::matrix::Dense<T, Rows, Cols>& dense) {
            if (dense.getRows() != Rows || dense.getCols() != Cols) {
                throw std::runtime_error("Matrix dimensions do not match template parameters");
            }

            for (size_t i = 0; i < dense.getRows(); ++i) {
                for (size_t j = 0; j < dense.getCols(); ++j) {
                    T value = dense(i, j);
                    if (value != 0) {
                        (*this)(i, j) = value;
                        ++sparsity;
                    }
                }
            }
            sparsity /= (Rows * Cols);
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols>::CSR(const COO<T, Rows, Cols>& coo) {
            std::map<size_t, std::map<size_t, T>> temp;
            for (const auto& item : coo.data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                temp[row][col] = value;
            }

            rowPtr.resize(Rows + 1);
            colInd.clear();
            values.clear();

            size_t nnz = 0;
            for (size_t i = 0; i < Rows; ++i) {
                rowPtr[i] = nnz;
                for (const auto& item : temp[i]) {
                    colInd.push_back(item.first);
                    values.push_back(item.second);
                    ++nnz;
                }
            }
            rowPtr[Rows] = nnz;
            sparsity = coo.sparsity;
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T, size_t Rows, size_t Cols> size_t CSR<T, Rows, Cols>::getSparsity() const {
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << (*this).sparsity << std::endl;
            }
            return sparsity;
        }

        template <typename T, size_t Rows, size_t Cols> stato::matrix::Dense<T, Rows, Cols> CSR<T, Rows, Cols>::toDense() const {
            Dense<T, Rows, Cols> dense;

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = rowPtr[i]; j < rowPtr[i + 1]; ++j) {
                    dense(i, colInd[j]) = values[j];
                }
            }
        }

        template <typename T, size_t Rows, size_t Cols> T CSR<T, Rows, Cols>::operator()(size_t i, size_t j) const {
            for (size_t k = rowPtr[i]; k < rowPtr[i + 1]; ++k) {
                if (colInd[k] == j) {
                    return values[k];
                }
            }

            return T{}; // Return default value if element not found
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols> CSR<T, Rows, Cols>::operator+(const CSR<T, Rows, Cols>& rhs) const {
            throw std::runtime_error("CSR matrix should not be added directly, convert to COO or Dense matrix first");
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols> CSR<T, Rows, Cols>::operator-(const CSR<T, Rows, Cols>& other) const {
            throw std::runtime_error("CSR matrix should not be subtracted directly, convert to COO or Dense matrix first");
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols> CSR<T, Rows, Cols>::operator*(const T& factor) const {
            for (size_t i = 0; i < values.size(); ++i) {
                values[i] *= factor;
            }
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols> CSR<T, Rows, Cols>::operator*(const CSR<T, Rows, Cols>& other) const {
            throw std::runtime_error("CSR matrix should not be multiplied directly, convert to COO or Dense matrix first");
        }

        template <typename T, size_t Rows, size_t Cols> CSR<T, Rows, Cols> operator*(const T& factor, const CSR<T, Rows, Cols> matrix) {
            return matrix * factor;
        }

        template <typename T, size_t Rows, size_t Cols> bool CSR<T, Rows, Cols>::operator==(const CSR<T, Rows, Cols>& other) const {
            if (Rows != other.getRows() || Cols != other.getCols()) {
                return false;
            }

            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = rowPtr[i]; j < rowPtr[i + 1]; ++j) {
                    if (std::abs(values[j] - other(i, colInd[j])) > std::numeric_limits<T>::epsilon() * 1000) {
                        std::cerr << "Mismatch at (" << i << ", " << colInd[j] << "): " << values[j] << " != " << other(i, colInd[j]) << std::endl;
                        printf("Diff = %10.20f\n", std::abs(values[j] - other(i, colInd[j])));
                        return false;
                    }
                }
            }

            return true;
        }

        // END Implementation of the CSR class

        // Implementation of the COO class

        template <typename T, size_t Rows, size_t Cols> COO<T, Rows, Cols>::COO() {
        }

        template <typename T, size_t Rows, size_t Cols>
        COO<T, Rows, Cols>::COO(const std::map<std::pair<size_t, size_t>, T>& data) : data(data), sparsity(data.size() / (Rows * Cols)) {
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T, size_t Rows, size_t Cols> COO<T, Rows, Cols>::COO(const std::string& filePath) {
            std::ifstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            std::string line;
            size_t      numNonZero = 0;

            // Read the header line
            std::getline(file, line);
            bool symmetric = false;
            if (line.substr(0, 32) != "%%MatrixMarket matrix coordinate") {
                throw std::runtime_error("Invalid matrix market format");
            } else if (line.find(" symmetric") != std::string::npos) {
                symmetric = true;
            }

            // Skip comment lines
            while (std::getline(file, line) && line[0] == '%') {
            }

            // Read the matrix size and number of non-zero elements
            std::istringstream iss(line);
            size_t             rows, cols;
            iss >> rows >> cols >> numNonZero;

            if (rows != Rows || cols != Cols) {
                throw std::runtime_error("Matrix dimensions do not match template parameters");
            }

            // Resize the COO matrix
            data.clear();

            // Read the non-zero elements
            if (symmetric) {
                while (std::getline(file, line)) {
                    std::istringstream iss(line);
                    size_t             row, col;
                    T                  value;
                    iss >> row >> col >> value;
                    data[std::make_pair(row - 1, col - 1)] += value;
                    data[std::make_pair(col - 1, row - 1)] += value;
                }
            } else {
                while (std::getline(file, line)) {
                    std::istringstream iss(line);
                    size_t             row, col;
                    T                  value;
                    iss >> row >> col >> value;
                    data[std::make_pair(row - 1, col - 1)] += value;
                }
            }

            (*this).sparsity = static_cast<double>(numNonZero / (Rows * Cols));

            file.close();
        }

        template <typename T, size_t Rows, size_t Cols> COO<T, Rows, Cols>::COO(const stato::matrix::Dense<T, Rows, Cols>& dense) {
            if (dense.getRows() != Rows || dense.getCols() != Cols) {
                throw std::runtime_error("Matrix dimensions do not match template parameters");
            }

            for (size_t i = 0; i < dense.getRows(); ++i) {
                for (size_t j = 0; j < dense.getCols(); ++j) {
                    T value = dense(i, j);
                    if (value != 0) {
                        (*this)(i, j) = value;
                        ++sparsity;
                    }
                }
            }
            sparsity /= (Rows * Cols);
            if (sparsity > 0.5) {
                std::cerr << "Warning: Matrix sparsity exceeds 50%. sparsity = " << sparsity << std::endl;
            }
        }

        template <typename T, size_t Rows, size_t Cols> stato::matrix::Dense<T, Rows, Cols> COO<T, Rows, Cols>::toDense() const {
            Dense<T, Rows, Cols> dense;
            for (const auto& item : data) {
                dense(item.first.first, item.first.second) = item.second;
            }
            return dense;
        }

        template <typename T, size_t Rows, size_t Cols> T COO<T, Rows, Cols>::operator()(size_t i, size_t j) const {
            auto it = data.find({i, j});
            if (it != data.end()) {
                return it->second;
            }
            return T{}; // Return default value if element not found
        }

        template <typename T, size_t Rows, size_t Cols> T& COO<T, Rows, Cols>::operator()(size_t i, size_t j) {
            return data[{i, j}];
        }

        template <typename T, size_t Rows, size_t Cols> COO<T, Rows, Cols> COO<T, Rows, Cols>::operator+(const COO<T, Rows, Cols>& rhs) const {
            COO<T, Rows, Cols> result;

            for (const auto& item : data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) = value;
            }

            for (const auto& item : rhs.data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) += value;
            }

            return result;
        }

        template <typename T, size_t Rows, size_t Cols> COO<T, Rows, Cols> COO<T, Rows, Cols>::operator-(const COO<T, Rows, Cols>& other) const {
            COO<T, Rows, Cols> result;

            for (const auto& item : data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) = value;
            }

            for (const auto& item : other.data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                result(row, col) -= value;
            }

            return result;
        }

        template <typename T, size_t Rows, size_t Cols> bool COO<T, Rows, Cols>::operator==(const COO<T, Rows, Cols>& other) const {
            for (const auto& item : data) {
                size_t row = item.first.first;
                size_t col = item.first.second;
                T      value = item.second;
                if (std::abs(value - other(row, col)) > std::numeric_limits<T>::epsilon() * 1000) {
                    std::cerr << "Mismatch at (" << row << ", " << col << "): " << value << " != " << other(row, col) << std::endl;
                    printf("Diff = %10.20f\n", std::abs(value - other(row, col)));
                    return false;
                }
            }

            return true;
        }

        template <typename T, size_t Rows, size_t Cols> COO<T, Rows, Cols> operator*(const T& factor, const COO<T, Rows, Cols> matrix) {
            return matrix * factor;
        }

        template <typename T, size_t Rows, size_t Cols>
        void COO<T, Rows, Cols>::writeToCOO(const std::string& filePath, const std::optional<int> precision) const {
            if (filePath.substr(filePath.size() - 4) != ".mtx") {
                throw std::invalid_argument("Matrix market file must have .mtx extension");
            }

            std::ofstream file(filePath);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open file");
            }

            if (precision.has_value()) {
                file.precision(*precision);
            } else {
                int exp;
                std::frexp(std::numeric_limits<T>::epsilon(), &exp);
                file.precision(-exp);
            }

            file << std::fixed;

            file << "%%MatrixMarket matrix coordinate real general" << std::endl;
            file << Rows << " " << Cols << " " << data.size() << std::endl;

            for (const auto& item : data) {
                file << item.first.first + 1 << " " << item.first.second + 1 << " " << item.second << std::endl;
            }

            file.close();
        }

        template <typename T, size_t Rows, size_t Cols> void COO<T, Rows, Cols>::show() const {
            for (size_t i = 0; i < Rows; ++i) {
                for (size_t j = 0; j < Cols; ++j) {
                    printf("%10.2f ", static_cast<float>((*this)(i, j)));
                }
                printf("\n");
            }
        }

        // END Implementation of the COO class

    } // namespace matrix::sparse
} // namespace stato

#endif // SPARSE_H