// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef SYMMETRIC_POSITIVE_DEFINITE_H
#define SYMMETRIC_POSITIVE_DEFINITE_H

#include "Builder/Dense.h"
#include "Builder/Sparse.h"

#include <random>
#include <vector>

namespace dyno::matrix {

    namespace dense {

        dyno::matrix::Dense<double> alphaDPlusBetaRRT(size_t n, double alpha, double beta) {
            std::random_device                     rd;
            std::mt19937                           gen(rd());
            std::uniform_real_distribution<double> dis(0.0, 1.0);

            dyno::matrix::Dense<double> A(n, n);
            std::vector<double>         r(n);
            std::vector<double>         d(n);

            for (size_t i0 = 0; i0 < n; ++i0) {
                r[i0] = dis(gen);
                d[i0] = dis(gen);
            }

            for (size_t i = 0; i < n; ++i) {
                for (size_t j = 0; j < n; ++j) {
                    if (i == j) {
                        A(i, j) = alpha * d[i] + beta * r[i] * r[i];
                    } else {
                        A(i, j) = beta * r[i] * r[j];
                    }
                }
            }

            return A;
        }

        dyno::matrix::Dense<double> hilbert(size_t n) {
            dyno::matrix::Dense<double> A(n, n);

            for (size_t i = 0; i < n; ++i) {
                for (size_t j = 0; j < n; ++j) {
                    A(i, j) = 1.0 / (i + j + 1);
                }
            }

            return A;
        }

    } // namespace dense

    namespace sparse {
        dyno::matrix::sparse::COO<double> diagDom(const size_t n, const size_t offset = 1) {
            std::random_device                     rd;
            std::mt19937                           gen(rd());
            std::uniform_real_distribution<double> dis(0.0, 1.0);

            std::map<std::pair<size_t, size_t>, double> data;

            for (size_t i0 = 0; i0 < n; ++i0) {
                std::vector<double> a(offset + 1);
                double              sumOffDiag = 0.0;
                for (size_t i1 = 0; i1 < offset + 1; ++i1) {
                    a[i1] = dis(gen);
                    sumOffDiag += std::abs(a[i1]);
                }
                sumOffDiag -= std::abs(a[0]);
                a[0] += sumOffDiag;
                for (size_t i1 = 0; i1 < offset + 1; ++i1) {
                    if (i0 + i1 < n) {
                        data[std::make_pair(i0, i1 + i0)] = a[i1];
                        data[std::make_pair(i0 + i1, i0)] = a[i1];
                    }
                }
            }

            return dyno::matrix::sparse::COO<double>(n, n, data);
        }
    } // namespace sparse
} // namespace dyno::matrix

namespace stato::matrix {
    namespace dense {

        template <size_t N> stato::matrix::Dense<double, N, N> alphaDPlusBetaRRT(double alpha, double beta) {
            std::random_device                     rd;
            std::mt19937                           gen(rd());
            std::uniform_real_distribution<double> dis(0.0, 1.0);

            stato::matrix::Dense<double, N, N> A;
            std::array<double, N>              r;
            std::array<double, N>              d;

            for (size_t i0 = 0; i0 < N; ++i0) {
                r[i0] = dis(gen);
                d[i0] = dis(gen);
            }

            for (size_t i = 0; i < N; ++i) {
                for (size_t j = 0; j < N; ++j) {
                    if (i == j) {
                        A(i, j) = alpha * d[i] + beta * r[i] * r[i];
                    } else {
                        A(i, j) = beta * r[i] * r[j];
                    }
                }
            }

            return A;
        }

        template <size_t N> stato::matrix::Dense<double, N, N> hilbert() {
            stato::matrix::Dense<double, N, N> A;

            for (size_t i = 0; i < N; ++i) {
                for (size_t j = 0; j < N; ++j) {
                    A(i, j) = 1.0 / (i + j + 1);
                }
            }

            return A;
        }
    } // namespace dense

    namespace sparse {

        template <size_t N, size_t Offset = 1> stato::matrix::sparse::COO<double, N, N> diagDom() {
            std::random_device                     rd;
            std::mt19937                           gen(rd());
            std::uniform_real_distribution<double> dis(0.0, 1.0);

            std::map<std::pair<size_t, size_t>, double> data;

            for (size_t i0 = 0; i0 < N; ++i0) {
                std::array<double, Offset + 1> a;
                double                         sumOffDiag = 0.0;
                for (size_t i1 = 0; i1 < Offset + 1; ++i1) {
                    a[i1] = dis(gen);
                    sumOffDiag += std::abs(a[i1]);
                }
                sumOffDiag -= std::abs(a[0]);
                a[0] += sumOffDiag;
                for (size_t i1 = 0; i1 < Offset + 1; ++i1) {
                    if (i0 + i1 < N) {
                        data[std::make_pair(i0, i1 + i0)] = a[i1];
                        data[std::make_pair(i0 + i1, i0)] = a[i1];
                    }
                }
            }

            return stato::matrix::sparse::COO<double, N, N>(data);
        }
    } // namespace sparse
} // namespace stato::matrix

#endif // SYMMETRIC_POSITIVE_DEFINITE_H