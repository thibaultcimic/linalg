// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef VECTOR_H
#define VECTOR_H

#include "Builder/Dense.h"
#include <cmath>
#include <vector>

namespace dyno {

    template <typename T> class Vector : public matrix::Dense<T> {
      public:
        Vector();
        Vector(size_t rows);
        Vector(size_t rows, T value);
        Vector(const std::vector<T>& data);
        ~Vector() = default;

        size_t size() const;

        T&        operator()(size_t i);
        const T&  operator()(size_t i) const;
        bool      operator==(const Vector<T>& rhs) const;
        Vector<T> operator+(const Vector<T>& rhs) const;
        Vector<T> operator-(const Vector<T>& rhs) const;
        Vector<T> operator*(const T& scalar) const;
        T         operator*(const Vector<T>& rhs) const;
        T         norm() const;
    };

    template <typename T> Vector<T>::Vector() : dyno::matrix::Dense<T>(0, 1) {
    }

    template <typename T> Vector<T>::Vector(size_t rows) : dyno::matrix::Dense<T>(rows, 1) {
    }

    template <typename T> Vector<T>::Vector(size_t rows, T value) : dyno::matrix::Dense<T>(value, rows, 1) {
    }

    template <typename T> Vector<T>::Vector(const std::vector<T>& data) : dyno::matrix::Dense<T>(data.size(), 1, data) {
    }

    template <typename T> size_t Vector<T>::size() const {
        return this->rows;
    }

    template <typename T> T& Vector<T>::operator()(size_t i) {
        return this->data[i];
    }

    template <typename T> const T& Vector<T>::operator()(size_t i) const {
        return this->data[i];
    }

    template <typename T> bool Vector<T>::operator==(const Vector<T>& rhs) const {
        if (this->rows != rhs.rows) {
            return false;
        }

        for (size_t i = 0; i < this->rows; ++i) {
            if (this->data[i] != rhs(i)) {
                return false;
            }
        }

        return true;
    }

    template <typename T> Vector<T> Vector<T>::operator+(const Vector<T>& rhs) const {
        if (this->rows != rhs.rows) {
            throw std::invalid_argument("Vector<T>::operator+ : incompatible dimensions");
        }

        Vector<T> result(this->rows);

        for (size_t i = 0; i < this->rows; ++i) {
            result(i) = this->data[i] + rhs(i);
        }

        return result;
    }

    template <typename T> Vector<T> Vector<T>::operator-(const Vector<T>& rhs) const {
        if (this->rows != rhs.rows) {
            throw std::invalid_argument("Vector<T>::operator- : incompatible dimensions");
        }

        Vector<T> result(this->rows);

        for (size_t i = 0; i < this->rows; ++i) {
            result(i) = this->data[i] - rhs(i);
        }

        return result;
    }

    template <typename T> Vector<T> Vector<T>::operator*(const T& scalar) const {
        Vector<T> result(this->rows);

        for (size_t i = 0; i < this->rows; ++i) {
            result(i) = this->data[i] * scalar;
        }

        return result;
    }

    template <typename T> Vector<T>& operator*(T& scalar, const Vector<T>& rhs) {
        return rhs * scalar;
    }

    template <typename T> T Vector<T>::operator*(const Vector<T>& rhs) const {
        if (this->rows != rhs.rows) {
            throw std::invalid_argument("Vector<T>::operator* : incompatible dimensions");
        }

        T result = T{};

        for (size_t i = 0; i < this->rows; ++i) {
            result += this->data[i] * rhs(i);
        }

        return result;
    }

    template <typename T> T Vector<T>::norm() const {
        return std::sqrt((*this) * (*this));
    }
} // namespace dyno

namespace stato {

    template <typename T, size_t N> class Vector : public matrix::Dense<T, N, 1> {
      public:
        Vector();
        Vector(T value);
        Vector(const std::array<T, N>& data);
        ~Vector() = default;

        constexpr size_t size() const;

        T&           operator()(size_t i);
        const T&     operator()(size_t i) const;
        bool         operator==(const Vector<T, N>& rhs) const;
        Vector<T, N> operator+(const Vector<T, N>& rhs) const;
        Vector<T, N> operator-(const Vector<T, N>& rhs) const;
        Vector<T, N> operator*(const T& scalar) const;
        T            operator*(const Vector<T, N>& rhs) const;
        T            norm() const;
    };

    template <typename T, size_t N> Vector<T, N>::Vector() : stato::matrix::Dense<T, N, 1>() {
    }

    template <typename T, size_t N> Vector<T, N>::Vector(T value) : stato::matrix::Dense<T, N, 1>(value) {
    }

    template <typename T, size_t N> Vector<T, N>::Vector(const std::array<T, N>& data) : stato::matrix::Dense<T, N, 1>(data) {
    }

    template <typename T, size_t N> constexpr size_t Vector<T, N>::size() const {
        return N;
    }

    template <typename T, size_t N> T& Vector<T, N>::operator()(size_t i) {
        return this->data[i];
    }

    template <typename T, size_t N> const T& Vector<T, N>::operator()(size_t i) const {
        return this->data[i];
    }

    template <typename T, size_t N> bool Vector<T, N>::operator==(const Vector<T, N>& rhs) const {
        for (size_t i = 0; i < N; ++i) {
            if (this->data[i] != rhs(i)) {
                return false;
            }
        }

        return true;
    }

    template <typename T, size_t N> Vector<T, N> Vector<T, N>::operator+(const Vector<T, N>& rhs) const {
        Vector<T, N> result{};

        for (size_t i = 0; i < N; ++i) {
            result(i) = this->data[i] + rhs(i);
        }

        return result;
    }

    template <typename T, size_t N> Vector<T, N> Vector<T, N>::operator-(const Vector<T, N>& rhs) const {
        Vector<T, N> result{};

        for (size_t i = 0; i < N; ++i) {
            result(i) = this->data[i] - rhs(i);
        }

        return result;
    }

    template <typename T, size_t N> Vector<T, N> Vector<T, N>::operator*(const T& scalar) const {
        Vector<T, N> result{};

        for (size_t i = 0; i < N; ++i) {
            result(i) = this->data[i] * scalar;
        }

        return result;
    }

    template <typename T, size_t N> Vector<T, N>& operator*(T& scalar, const Vector<T, N>& rhs) {
        return rhs * scalar;
    }

    template <typename T, size_t N> T Vector<T, N>::operator*(const Vector<T, N>& rhs) const {
        T result = T{};

        for (size_t i = 0; i < N; ++i) {
            result += this->data[i] * rhs(i);
        }

        return result;
    }

    template <typename T, size_t N> T Vector<T, N>::norm() const {
        return std::sqrt((*this) * (*this));
    }
} // namespace stato

#endif // VECTOR_H