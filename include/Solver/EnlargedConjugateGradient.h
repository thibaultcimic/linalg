// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef CONJUGATE_GRADIENT_H
#define CONJUGATE_GRADIENT_H

#include "IterativeSolver.h"

namespace solvers {

    template <typename M> class EnlargedConjugateGradient : public IterativeSolver<M> {

      using T = typename M::scalarType;

      public:
        EnlargedConjugateGradient(const M&               A,
                                  const dyno::Vector<T>& rhs,
                                  const dyno::Vector<T>& initialGuess,
                                  size_t                 splitFactor = 8,
                                  size_t                 maxIter = 1000,
                                  T                      tol = static_cast<T>(1e-6))
            : IterativeSolver<T, M>(A, rhs, initialGuess, maxIter, tol) {
        }

        T initialize() override;

        dyno::Vector<T> run() override;

        void iterate(dyno::Vector<T>& searchDirection) override;

        bool stop(const T rhsNorm) override;

      private:
        size_t splitFactor;
    };

    template <typename T, typename M> T EnlargedConjugateGradient<T, M>::initialize() {
        this->iterationCount = 0;
        this->residual = this->rhs - this->A * this->initialGuess;
        this->residualNorms.resize(this->maxIterations + 1);
        this->residualNorms[this->iterationCount] = this->residual.norm();
        this->solution = this->initialGuess;
        return this->rhs.norm();
    }

    template <typename T, typename M> dyno::Vector<T> EnlargedConjugateGradient<T, M>::run() {
        auto            rhsNorm = initialize();
        dyno::Vector<T> searchDirection = this->residual;

        do {
            iterate(searchDirection);
        } while (!stop(rhsNorm));

        return this->solution;
    }

    template <typename T, typename M> void EnlargedConjugateGradient<T, M>::iterate(dyno::Vector<T>& searchDirection) {
        // alpha = (r_k, r_k) / (p_k, A * p_k)
        auto Ap = this->A * searchDirection;
        auto oldResidualNorm = this->residualNorms[this->iterationCount];
        T    alpha = (oldResidualNorm * oldResidualNorm) / (searchDirection * Ap);

        this->solution = this->solution + searchDirection * alpha;
        this->residual = this->residual - Ap * alpha;

        // beta = (r_{k+1}, r_{k+1}) / (r_k, r_k)
        auto newResidualNorm = this->residual.norm();
        this->residualNorms[this->iterationCount + 1] = newResidualNorm;
        T beta = (newResidualNorm * newResidualNorm) / (oldResidualNorm * oldResidualNorm);

        // printf("Iteration %zu: alpha: %10.20f, oldResidualNorm: %f, newResidualNorm: %f, beta: %10.20f\n",
        //        this->iterationCount, alpha, oldResidualNorm, newResidualNorm, beta);

        searchDirection = this->residual + searchDirection * beta;
    }

    template <typename T, typename M> bool EnlargedConjugateGradient<T, M>::stop(const T rhsNorm) {
        ++(this->iterationCount);

        if (this->residualNorms[this->iterationCount] < this->tolerance * rhsNorm || this->iterationCount % 50 == 0) {
            auto exactResidual = this->rhs - this->A * this->solution;
            this->residualNorms[this->iterationCount] = exactResidual.norm();
            if (exactResidual.norm() < this->tolerance * rhsNorm) {
                printf("CG converged in %zu iterations with a residual norm of %10.20f\n", this->iterationCount, exactResidual.norm());
                return true;
            }
        } else if (this->iterationCount >= this->maxIterations && this->residualNorms[this->iterationCount] > this->tolerance * rhsNorm) {
            printf("CG did not converge in %zu iterations with a residual norm of %10.20f\n", this->iterationCount, this->residualNorms[this->iterationCount]);
            return true;
        }

        return false;
    }
} // namespace solvers

#endif // CONJUGATE_GRADIENT_H
