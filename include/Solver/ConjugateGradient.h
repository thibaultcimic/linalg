// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef CONJUGATE_GRADIENT_H
#define CONJUGATE_GRADIENT_H

#include "IterativeSolver.h"

namespace dyno::solvers {

    template <typename M> class ConjugateGradient : public IterativeSolver<M> {

      using T = typename M::scalarType;

      public:
        ConjugateGradient(const M& A, const dyno::Vector<T>& rhs, const dyno::Vector<T>& initialGuess, size_t maxIter = 1000, T tol = static_cast<T>(1e-6))
            : IterativeSolver<M>(A, rhs, initialGuess, maxIter, tol) {
        }

        T initialize() override;

        dyno::Vector<T> run() override;

        void iterate(dyno::Vector<T>& searchDirection) override;

        bool stop(const T rhsNorm) override;
    };

    template <typename M> ConjugateGradient<M>::T ConjugateGradient<M>::initialize() {
        this->iterationCount = 0;
        this->residual = this->rhs - this->A * this->initialGuess;
        this->residualNorms.resize(this->maxIterations + 1);
        this->residualNorms[this->iterationCount] = this->residual.norm();
        this->solution = this->initialGuess;
        return this->rhs.norm();
    }

    template <typename M> dyno::Vector<typename ConjugateGradient<M>::T> ConjugateGradient<M>::run() {
        auto            rhsNorm = initialize();
        dyno::Vector<T> searchDirection = this->residual;

        do {
            iterate(searchDirection);
        } while (!stop(rhsNorm));

        return this->solution;
    }

    template <typename M> void ConjugateGradient<M>::iterate(dyno::Vector<T>& searchDirection) {
        // alpha = (r_k, r_k) / (p_k, A * p_k)
        auto Ap = this->A * searchDirection;
        auto oldResidualNorm = this->residualNorms[this->iterationCount];
        T    alpha = (oldResidualNorm * oldResidualNorm) / (searchDirection * Ap);

        this->solution = this->solution + searchDirection * alpha;
        this->residual = this->residual - Ap * alpha;

        // beta = (r_{k+1}, r_{k+1}) / (r_k, r_k)
        auto newResidualNorm = this->residual.norm();
        this->residualNorms[this->iterationCount + 1] = newResidualNorm;
        T beta = (newResidualNorm * newResidualNorm) / (oldResidualNorm * oldResidualNorm);

        // printf("Iteration %zu: alpha: %10.20f, oldResidualNorm: %f, newResidualNorm: %f, beta: %10.20f\n",
        //        this->iterationCount, alpha, oldResidualNorm, newResidualNorm, beta);

        searchDirection = this->residual + searchDirection * beta;
    }

    template <typename M> bool ConjugateGradient<M>::stop(const T rhsNorm) {
        ++(this->iterationCount);

        if (this->residualNorms[this->iterationCount] < this->tolerance * rhsNorm || this->iterationCount % 50 == 0) {
            auto exactResidual = this->rhs - this->A * this->solution;
            this->residualNorms[this->iterationCount] = exactResidual.norm();
            if (exactResidual.norm() < this->tolerance * rhsNorm) {
                printf("CG converged in %zu iterations with a residual norm of %10.20f\n", this->iterationCount, exactResidual.norm());
                return true;
            }
        } else if (this->iterationCount >= this->maxIterations && this->residualNorms[this->iterationCount] > this->tolerance * rhsNorm) {
            printf("CG did not converge in %zu iterations with a residual norm of %10.20f\n", this->iterationCount, this->residualNorms[this->iterationCount]);
            return true;
        }

        return false;
    }
} // namespace dyno::solvers

namespace stato::solvers {

    template <typename M> class ConjugateGradient : public IterativeSolver<M> {
      public:

        using T = typename M::scalarType;
        static constexpr size_t Rows = M::rowsSize;
        static constexpr size_t Cols = M::colsSize;

        ConjugateGradient(const M& A, const stato::Vector<T, Cols>& rhs, const stato::Vector<T, Cols>& initialGuess, size_t maxIter = 1000, double tol = 1e-6)
            : IterativeSolver<M>(A, rhs, initialGuess, maxIter, tol) {}

        T initialize() override;

        stato::Vector<T, Cols> run() override;

        void iterate(stato::Vector<T, Cols>& searchDirection) override;

        bool stop(const T rhsNorm) override;
    };

    template <typename M> ConjugateGradient<M>::T ConjugateGradient<M>::initialize() {
        this->iterationCount = 0;
        this->residual = this->rhs - this->A * this->initialGuess;
        this->residualNorms.resize(this->maxIterations + 1);
        this->residualNorms[this->iterationCount] = this->residual.norm();
        this->solution = this->initialGuess;
        return this->rhs.norm();
    }

    template <typename M> stato::Vector<typename ConjugateGradient<M>::T, ConjugateGradient<M>::Cols> ConjugateGradient<M>::run() {
        auto                rhsNorm = initialize();
        stato::Vector<T, Cols> searchDirection = this->residual;

        do {
            iterate(searchDirection);
        } while (!stop(rhsNorm));

        return this->solution;
    }

    template <typename M> void ConjugateGradient<M>::iterate(stato::Vector<T, Cols>& searchDirection) {
        // alpha = (r_k, r_k) / (p_k, A * p_k)
        auto Ap = this->A * searchDirection;
        auto oldResidualNorm = this->residualNorms[this->iterationCount];
        T    alpha = (oldResidualNorm * oldResidualNorm) / (searchDirection * Ap);

        this->solution = this->solution + searchDirection * alpha;
        this->residual = this->residual - Ap * alpha;

        // beta = (r_{k+1}, r_{k+1}) / (r_k, r_k)
        auto newResidualNorm = this->residual.norm();
        this->residualNorms[this->iterationCount + 1] = newResidualNorm;
        T beta = (newResidualNorm * newResidualNorm) / (oldResidualNorm * oldResidualNorm);

        searchDirection = this->residual + searchDirection * beta;
    }

    template <typename M> bool ConjugateGradient<M>::stop(const T rhsNorm) {
        ++(this->iterationCount);

        if (this->residualNorms[this->iterationCount] < this->tolerance * rhsNorm || this->iterationCount % 50 == 0) {
            auto exactResidual = this->rhs - this->A * this->solution;
            this->residualNorms[this->iterationCount] = exactResidual.norm();
            if (exactResidual.norm() < this->tolerance * rhsNorm) {
                printf("CG converged in %zu iterations with a residual norm of %10.20f\n", this->iterationCount, exactResidual.norm());
                return true;
            }
        } else if (this->iterationCount >= this->maxIterations && this->residualNorms[this->iterationCount] > this->tolerance * rhsNorm) {
            printf("CG did not converge in %zu iterations with a residual norm of %10.20f\n", this->iterationCount, this->residualNorms[this->iterationCount]);
            return true;
        }

        return false;
    }
} // namespace stato::solvers

#endif // CONJUGATE_GRADIENT_H
