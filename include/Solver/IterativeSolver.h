// This file is part of Lina, which is licensed under the GNU General Public License version 3.
// See the COPYING file in the root directory of this project or <http://www.gnu.org/licenses/gpl-3.0.html>.

#ifndef ITERATIVE_SOLVER_H
#define ITERATIVE_SOLVER_H

#include "Builder/Dense.h"
#include "Builder/Sparse.h"
#include "Builder/Vector.h"

namespace dyno::solvers {
    template <typename M> class IterativeSolver {

      using T = typename M::scalarType;

      protected:
        M               A;
        dyno::Vector<T> rhs;
        dyno::Vector<T> initialGuess;
        size_t          maxIterations;
        T               tolerance;
        size_t          problemSize;
        size_t          iterationCount;
        dyno::Vector<T> residual;
        std::vector<T>  residualNorms;
        dyno::Vector<T> solution;

      public:
        IterativeSolver(const M& A, const dyno::Vector<T>& rhs, const dyno::Vector<T>& initialGuess, size_t maxIter = 1000, T tol = static_cast<T>(1e-6))
            : A(A), rhs(rhs), initialGuess(initialGuess), maxIterations(maxIter), tolerance(tol) {

            if (A.getRows() != A.getCols()) {
                throw std::invalid_argument("Matrix must be square.");
            }

            if (A.getRows() != rhs.size()) {
                throw std::invalid_argument("Matrix and rhs vector dimensions do not match.");
            }

            problemSize = A.getRows();
            iterationCount = 0;
        }

        virtual ~IterativeSolver() {
        }

        virtual T initialize() = 0;

        virtual dyno::Vector<T> run() = 0;

        virtual void iterate(dyno::Vector<T>& searchDirection) = 0;

        virtual bool stop(const T rhsNorm) = 0;
    };
} // namespace dyno::solvers

namespace stato::solvers {
    template <typename M> class IterativeSolver {

    using T = typename M::scalarType;
    static constexpr size_t Rows = M::rowsSize;
    static constexpr size_t Cols = M::colsSize;

    protected:
        M                   A;
        stato::Vector<T, Cols> rhs;
        stato::Vector<T, Cols> initialGuess;
        size_t              maxIterations;
        T                   tolerance;
        size_t              problemSize;
        size_t              iterationCount;
        stato::Vector<T, Cols> residual;
        std::vector<T>      residualNorms;
        stato::Vector<T, Cols> solution;

    public:
        IterativeSolver(
            const M& A, const stato::Vector<T, Cols>& rhs, const stato::Vector<T, Cols>& initialGuess, size_t maxIter = 1000, double tol = 1e-6)
            : A(A), rhs(rhs), initialGuess(initialGuess), maxIterations(maxIter), tolerance(tol) {

            if (A.getRows() != A.getCols()) {
                throw std::invalid_argument("Matrix must be square.");
            }

            if (A.getRows() != rhs.size()) {
                throw std::invalid_argument("Matrix and rhs vector dimensions do not match.");
            }

            problemSize = A.getRows();
            iterationCount = 0;
        }

        virtual ~IterativeSolver() {
        }

        virtual T initialize() = 0;

        virtual stato::Vector<T, Cols> run() = 0;

        virtual void iterate(stato::Vector<T, Cols>& searchDirection) = 0;

        virtual bool stop(const T rhsNorm) = 0;
    };
} // namespace stato::solvers

#endif // ITERATIVE_SOLVER_H